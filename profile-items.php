<?php


$stmta = $db->prepare(selectInfoProfesional());
$stmta->execute(array(2, 1));
$result_profesional = $stmta->fetchAll();

?>

<!--item profesional-->
<?php foreach ($result_profesional as  $prof) {
    $stmt_esp = $db->prepare(selectCategoriaProfesional());
    $stmt_esp->execute(array($prof["id_usuario"]));
    $result_categoria = $stmt_esp->fetchAll();

?>
    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6 prof-item mb-3">
        <div class="card shadow-none rounded">
            <a href="single.php?u=<?php echo $prof["id_usuario"]; ?>" class="over-link">&nbsp;</a>
            <div class="row no-gutters">
                <div class="col-md-5 bg-cover prof-img rounded" style="background-image: url(<?php echo $direccion_base . "/uploads/" . md5($prof["id_usuario"]) . "/" . $prof["imagen_perfil"]; ?>);">
                    &nbsp;
                </div>
                <div class="col-md-7">
                    <div class="card-body">
                        
                        <h3 class="card-title font-bold"><?php echo ($prof["tx_nombre"] . ' ' . $prof["tx_apellido"]); ?></h3>
                        <p class="card-text"><small><?php echo substr($prof['tx_descripcion'], 0, 200); ?>...</small></p>
                        <?php foreach ($result_categoria as $cat) {
                            if ($cat["tipo_categoria"] == 1) {

                        ?>
                                <span class="badge badge-pill"><?php echo utf8_encode($cat["nb_categoria"]); ?></span>
                        <?php
                            }
                        } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

