<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok" && $_SESSION["id_rol"] == 3) {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);
$meses = [
    "01" => "enero",
    "02" => "febrero",
    "03" => "marzo",
    "04" => "abril",
    "05" => "mayo",
    "06" => "junio",
    "07" => "julio",
    "08" => "agosto",
    "09" => "septiembre",
    "10" => "octubre",
    "11" => "noviembre",
    "12" => "diciembre",
];


$fecha = date('Y-m-d');

$stmta = $db->prepare(selectReservaCliente());
$stmta->execute(array($_SESSION["id_usuario"], $fecha));
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);
$cant_results = count($result);
//var_dump($result);
?>
<?php include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-dark bg-gradient-primary-to-secondary mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="calendar"></i></div>
                                        Mis horas
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!--Informacion personal-->
                            <div id="infhoras">
                                <div class="card mb-4">
                                    <div class="card-header">Próximas horas</div>
                                    <div class="card-body">


                                        <?php if ($cant_results == 0) {  ?>
                                            <!--sin horas
                                        <span class="text-muted text-sm">[Cuando el paciente no tiene horas próximas]</span>-->
                                            <div class="sbp-preview mb-3">
                                                <div class="sbp-preview-content">
                                                    Sin horas
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <!-- con horas
                                                <span class="text-muted text-sm">[Cuando el paciente horas próximas]</span>-->

                                            <div class="datatable">
                                                <table class="table table-bordered table-hover" id="tableReportes" width="100%" cellspacing="0">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Consulta</th>
                                                            <th>Nombre Profesional</th>
                                                            <th>Mes Consulta</th>
                                                            <th>Día Consulta</th>
                                                            <th>Hora Consulta</th>
                                                            <th>Previsión</th>
                                                            <th>Total Pagado</th>
                                                            <th>Cancelar Hora</th>
                                                        </tr>
                                                    </thead>
                                                    <tfoot>
                                                        <tr>
                                                            <th>ID Consulta</th>
                                                            <th>Nombre Profesional</th>
                                                            <th>Mes Consulta</th>
                                                            <th>Día Consulta</th>
                                                            <th>Hora Consulta</th>
                                                            <th>Previsión</th>
                                                            <th>Total Pagado</th>
                                                            <th>Cancelar Hora</th>
                                                        </tr>
                                                    </tfoot>
                                                    <tbody>
                                                        <?php foreach ($result as $res) {  ?>
                                                        <tr>
                                                            <td><?php echo $res["cod_trans"]; ?></td>
                                                            <td><?php echo utf8_encode($res["tx_nombre"].' '.$res["tx_apellido"]); ?></td>
                                                            <td><?php   $mes = explode("-", $res["fecha_reserva"])[1]; echo $meses[$mes]; ?></td>
                                                            <td><?php $dia = explode("-", $res["fecha_reserva"])[2]; echo $dia; ?></td>
                                                            <td><?php echo ($res["hora_inicio"].' a '.$res["hora_fin_visual"]); ?></td>
                                                            <td><?php echo utf8_encode($res["nb_categoria"]); ?></td>
                                                            <td><?php echo ($res["total_consulta"]); ?></td>
                                                            <td>
                                                                <button class="btn btn-danger btn-xs" type="button" data-fancybox data-src="#cancel01" data-modal="true" href="javascript:;">Cancelar hora</button>
                                                                <div style="display: none;min-width:90vw;max-width:100vw;padding:0;" id="cancel01" class="card rounded">
                                                                    <div class="card-header">
                                                                        <h2 class="mb-0">Cancelar hora</h2>
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div class="alert alert-info" role="alert">
                                                                            <p class="mb-0 pb-0">Lamentamos que hayas decidido cancelar. <br>La cancelación no implica devolución, si deseas apelar a un reembolso escríbenos a contacto@grupoconscientia.cl</p>
                                                                        </div>

                                                                        <form>
                                                                            <div class="form-group d-flex justify-content-between w-100 align-items-center">
                                                                                <a href="#" data-fancybox-close>Cerrar ventana</a>
                                                                                <button data-fancybox-close class="btn btn-danger btn-lg">Proceder</button>
                                                                            </div>
                                                                            <small class="d-block text-right text-muted">[Dispara Mail [CONSULTA CANCELADA - 04A]]</small>

                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php } ?>



                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>