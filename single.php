<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php
session_start();
include 'commons/funciones.php';
include 'commons/schema.php';

$db = connectToDB($dbData);

$stmta = $db->prepare(selectInfoProfesionalId());
$stmta->execute(array(2, 1, $_GET["u"]));
$result_profesional = $stmta->fetch(PDO::FETCH_ASSOC);


$stmt_esp = $db->prepare(selectCategoriaProfesional());
$stmt_esp->execute(array($_GET["u"]));
$result_categoria = $stmt_esp->fetchAll();

$stmt_days_working = $db->prepare(selectDaysActiveProfesional());
$stmt_days_working->execute(array($_GET["u"]));
$result_days_working = $stmt_days_working->fetchAll();
$working_days = array();
$cadena_dias = "";
foreach ($result_days_working as $index => $days) {
    $id_dia = (int)$days["id_dia"];
    if ($id_dia == 7) {
        $id_dia = 0;
    }

    if ($index === array_key_last($result_days_working)) {
        $cadena_dias .= $id_dia;
    } else {
        $cadena_dias .= $id_dia . ',';
    }


    array_push($working_days, $id_dia);
}

if (isset($_GET["u"])) {


    include 'header.php' ?>
    <link href='fullcalendar/css/main.css' rel='stylesheet' />
    <script src='fullcalendar/js/main.js'></script>

    <?php include 'topnav-front.php' ?>
    <style>
.picker__list-item.picker__list-item--disabled{
    cursor:no-drop !important;
    background-color: rgba(255, 0, 0, 0.1) !important;
}
</style>
    <!-- Inicio contenido home -->

    <body id="single" class="nav-fixed">

        <div class="container mt-4">
            <div class="main-body">

                <!-- Breadcrumb -->
                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb bg-white">
                        <li class="breadcrumb-item"><a href="profesionales.php"><i data-feather="chevrons-left"></i> Volver</a></li>
                    </ol>
                </nav>
                <!-- /Breadcrumb -->

                <div class="row gutters-sm">
                    <div class="col-md-4 mb-3">
                        <div class="card shadow-none">
                            <div class="card-body">
                                <div class="d-flex flex-column align-items-center text-center">
                                    <div class="d-block mx-auto m-4 bg-bermuda rounded-circle bg-cover" style="width:300px; height:300px; background-image:url(<?php echo $direccion_base . "/uploads/" . md5($result_profesional["id_usuario"]) . "/" . $result_profesional["imagen_perfil"]; ?>);">&nbsp;</div>
                                    <div class="mt-3">
                                        <h1 class="text-bold mb-4 display-4">
                                            <?php echo utf8_encode($result_profesional["tx_nombre"] . ' ' . $result_profesional["tx_apellido"]); ?>
                                        </h1>
                                        <hr>
                                        <ul class="list-group">
                                            <li class="list-group-item d-flex justify-content-between align-items-center border-0 p-0">
                                                <small>Registro MINSAL</small>
                                                <span class="badge badge-primary badge-pill">01234567890</span>
                                            </li>
                                        </ul>
                                        <hr>
                                        <?php foreach ($result_categoria as $cat) {
                                            if ($cat["tipo_categoria"] == 1) {
                                        ?>
                                                <span class="badge badge-pill badge-primary"><?php echo utf8_encode($cat["nb_categoria"]); ?></span>

                                        <?php
                                            }
                                        } ?>

                                        <hr>
                                        <p class="font-size-sm text-left"><?php echo utf8_encode($result_profesional["tx_descripcion"]) ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="card mb-3 shadow-none">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap bg-bermuda">
                                    <h6 class="mb-0"><i data-feather="wifi"></i> Modalidad de Atención</h6>
                                    <span>Online</span>
                                </li>
                            </ul>
                        </div>
                        <div class="card mb-3 shadow-none">
                            <div class="card-body">
                                <h1>Reserva una hora <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok" && $_SESSION["id_rol"] == 3) {
                                                        } else {
                                                            echo '<small class="text-muted">(Para reservar una hora debes iniciar sesión)</small>';
                                                        }  ?></h1>
                                <hr>
                                <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok" && $_SESSION["id_rol"] == 3) { ?>
                                    <form action="checkout.php" method="POST">
                                        <span class="text-muted">A continuación se muestran los días disponibles para reservar. </span>
                                        <?php //var_dump($working_days); echo $cadena_dias; 
                                        ?>
                                        <hr>
                                        <a href="#" id="back-to-calendar">Volver a calendario</a>
                                        <input type="text" id="working_days" value="<?php echo $cadena_dias; ?>" hidden>
                                        <input type="text" id="id_profesional" name="id_profesional" value="<?php echo (int)$_GET["u"]; ?>" hidden>
                                        <input type="text" id="field-day" name="field-day" value="" hidden>
                                        <div id='calendar'></div>

                                        <div class="row mt-3 mb-3" id="campos-fecha-hora">
                                            <input type="date" value="" name="date-reservation" id="date-reservation" hidden>
                                            <label for="" id="label-hora_reserva">Seleccione una hora:</label>
                                            <input class="hora timepicker form-control" name="hora_reserva" id="hora_reserva" value="" hidden>



                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="" class="lblform"> Seleccione un servicio</label>
                                                <select name="select-categoria" id="select-categoria" class="form-control mb-3">
                                                    <option value="" selected>Seleccione</option>
                                                    <?php foreach ($result_categoria as $cat) {
                                                        if ($cat["tipo_categoria"] == 1) {


                                                    ?>
                                                            <option value="<?php echo $cat["id_categoria"]; ?>"><?php echo utf8_encode($cat["nb_categoria"]); ?></option>

                                                    <?php
                                                        }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <label for="" class="lblform"> Seleccione una previsión</label>
                                                <select name="select-prevision" id="select-prevision" class="form-control mb-3">
                                                    <option value="" selected>Seleccione</option>
                                                    <?php foreach ($result_categoria as $cat) {
                                                        if ($cat["tipo_categoria"] == 2) {


                                                    ?>
                                                            <option value="<?php echo $cat["id_categoria"]; ?>"><?php echo utf8_encode($cat["nb_categoria"]); ?></option>

                                                    <?php
                                                        }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-12">
                                                <label for="" class="lblform"> Seleccione un motivo</label>
                                                <select name="select-motivo" id="select-motivo" class="form-control lblform mb-3">
                                                    <option value="" selected>Seleccione</option>
                                                    <?php foreach ($result_categoria as $cat) {
                                                        if ($cat["tipo_categoria"] == 3) {


                                                    ?>
                                                            <option value="<?php echo $cat["id_categoria"]; ?>"><?php echo utf8_encode($cat["nb_categoria"]); ?></option>

                                                    <?php
                                                        }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok" && $_SESSION["id_rol"] == 3) { ?>
                                            <div class="card-footer text-center">

                                                <button type="submit" class="btn btn-lg btn-primary lblform">Proceder al checkout</button>
                                            </div>
                                        <?php } ?>
                                    </form>
                                <?php } else { ?>
                                    <div class="row mt-3">
                                        <!--registrarse-->
                                        <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 mb-3">
                                            <div class="card shadow-none mb-4">
                                                <div class="card-header">
                                                    <h3 class="mb-0">Registrarse</h3>

                                                </div>
                                                <div class="card-body">
                                                    <!-- Component Preview-->
                                                    <form>
                                                        <div class="form-group row">
                                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                                <label><strong>Nombre</strong></label>
                                                                <input class="form-control" type="text" name="nombre" id="nombre" required />
                                                            </div>
                                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                                <label><strong>Apellidos</strong></label>
                                                                <input class="form-control" type="text" name="apellido" id="apellido" required />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                                <label><strong>RUT</strong></label>
                                                                <input id="rut" class="form-control" type="text" name="rut" required />
                                                            </div>
                                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                                <label><strong>Correo Electrónico</strong></label>
                                                                <input class="form-control" type="email" name="correo" id="correo" required />
                                                            </div>
                                                            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                                <label><strong>Teléfono</strong></label>
                                                                <input class="form-control" type="tel" required />
                                                                <small class="text-muted">Ej: +569...</small>
                                                            </div>
                                                        </div>
                                                        <p class="text-muted"><small> Al registrarte estás de acuerdo con los <a href="terminos-y-condiciones.php" target="_blank">términos y condiciones</a></small></p>
                                                        <span id="span-val-register" style="color:red"></span>
                                                        <span id="span-val-register-success" style="color:green"></span>
                                                        <div class="card-footer text-center">
                                                            <button type="button" class="btn btn-lg btn-primary btn-register">Registrar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!--ingresar-->
                                        <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mb-3">
                                            <div class="card shadow-none mb-4">
                                                <div class="card-header">
                                                    <h3 class="mb-0">Ingresar</h3>

                                                </div>
                                                <div class="card-body">
                                                    <!-- Component Preview-->
                                                    <form action="val_login.php" method="POST">
                                                        <div class="col-12 mb-3">
                                                            <label><strong>Correo Electrónico</strong></label>
                                                            <input class="form-control" type="email" name="correo" required />
                                                        </div>
                                                        <div class="col-12 mb-3">
                                                            <label><strong>Contraseña</strong></label>
                                                            <input class="form-control" type="password" name="pass" required />
                                                        </div>
                                                        <div class="form-group row">
                                                            <div class="col-6 mb-3 pl-4">
                                                                <a href="recordar-contrasena.php" target="_blank"><small>Recuperar mi contraseña</small></a>
                                                            </div>
                                                            <div class="col-6 mb-3 text-right">
                                                                <button type="submit" class="btn btn-primary mr-2" data-dismiss="modal">Entrar</button>
                                                            </div>
                                                        </div>

                                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                            Acceso Incorrecto
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>


                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                <?php } ?>
                            </div>

                        </div>



                    </div>
                </div>

            </div>
        </div>


        <?php include 'footer-line.php' ?>

        <?php include 'footer.php' ?>

        <script>
            // SCRIPT para reserva
            var week_days = [0, 1, 2, 3, 4, 5, 6];
            var days_from_profesional = $("#working_days").val().split(",");
            var work_calendar = [];
            var active_days = [];
            var disable_all_times = []; //{from:[0,0],to:[23,30]}
            $("#date-reservation").val("");
            $("#hora_reserva").val("");
            $("#label-hora_reserva").fadeOut();
            $("#back-to-calendar").fadeOut();
            $("#select-categoria").fadeOut();
            $("#select-prevision").fadeOut();
            $(".lblform").fadeOut();
            //instanciar_timepicker_front();
            function instanciar_timepicker_front(fechas) {
                console.log("instanciar timepicker");

                $('.timepicker').each(function() {
                    console.dir(fechas);
                    $(this).pickatime({
                        format: 'HH:i',
                        disable: fechas,
                        onOpen: function() {
                            console.log('Opened up');
                            getReservedTimes();
                        },
                    });
                });
            }
            for (let index = 0; index < days_from_profesional.length; index++) {
                work_calendar.push(parseInt(days_from_profesional[index]));
            }
            var diff = $(week_days).not(work_calendar).get();
            $("#back-to-calendar").on('click', function() {
                $("#calendar").fadeIn();
                $("#back-to-calendar").fadeOut();
                $("#hora_reserva").hide();
                $("#label-hora_reserva").fadeOut();
                $("#select-categoria").fadeOut();
                $("#select-prevision").fadeOut();
                $(".lblform").fadeOut();
            });

            document.addEventListener('DOMContentLoaded', function() {
                var calendarEl = document.getElementById('calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                    dateClick: function(info) {
                        //alert('Date: ' + info.dateStr);
                        console.dir(info);
                        $("#hora_reserva").val("");
                        var day = info.date.getDay(); // number 0-6 with Sunday as 0 and Saturday as 6
                        var dayEl = info.dayEl.className;
                        console.log(dayEl);
                        var is_future = dayEl.indexOf("fc-day-future");
                        var is_now = dayEl.indexOf("fc-day-today");
                        var is_dis = dayEl.indexOf("dis");
                        console.log(is_dis);

                        if (is_dis > -1) {
                            console.log("dia inhabilitado");
                        } else {

                            //alert(info.dateStr);
                            if (day === 0) {
                                day = 7;
                            }
                            $("#field-day").val(day);
                            $("#hora_reserva").remove();
                            $("#calendar").fadeOut();
                            $("#label-hora_reserva").fadeIn();
                            $("#back-to-calendar").fadeIn();
                            $("#select-categoria").fadeIn();
                            $("#select-prevision").fadeIn();
                            $(".lblform").fadeIn();
                            addDateToField(info.dateStr, day);
                        }



                    },
                    locale: 'es',
                    firstDay: 1,
                    //hiddenDays: diff
                });
                calendar.render();
                $(".fc-day").each(function() {
                    if ($(this).hasClass("fc-day-past")) {
                        $(this).css("background-color", "tomato");
                        $(this).addClass("dis");
                    }
                    var dia = $(this).attr("class").split(" ")[2].split("-")[2];
                    //console.log(dia);
                    var arrDays = {
                        "mon": 1,
                        "tue": 2,
                        "wed": 3,
                        "thu": 4,
                        "fri": 5,
                        "sat": 6,
                        "sun": 0
                    };
                    //console.log(arrDays[dia]);
                    var existDay = $.inArray(arrDays[dia], diff);
                    //console.log(existDay);
                    if (existDay !== -1) {
                        $(this).css("background-color", "tomato");
                        $(this).addClass("dis");
                    }

                });
                console.dir(diff);
                $('.fc-next-button').on('click', function() { // Para renderizar los campos validos cuando avance de mes
                    calendar.next();
                    $(".fc-day").each(function() {
                        if ($(this).hasClass("fc-day-past")) {
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }
                        var dia = $(this).attr("class").split(" ")[2].split("-")[2];
                        //console.log(dia);
                        var arrDays = {
                            "mon": 1,
                            "tue": 2,
                            "wed": 3,
                            "thu": 4,
                            "fri": 5,
                            "sat": 6,
                            "sun": 0
                        };
                        //console.log(arrDays[dia]);
                        var existDay = $.inArray(arrDays[dia], diff);
                        //console.log(existDay);
                        if (existDay !== -1) {
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }

                    });
                });

                $('.fc-prev-button').on('click', function() { // Para renderizar los campos validos cuando retroceda de mes
                    calendar.prev();
                    $(".fc-day").each(function() {
                        if ($(this).hasClass("fc-day-past")) {
                            console.log("es pasado");
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }
                        var dia = $(this).attr("class").split(" ")[2].split("-")[2];
                        //console.log(dia);
                        var arrDays = {
                            "mon": 1,
                            "tue": 2,
                            "wed": 3,
                            "thu": 4,
                            "fri": 5,
                            "sat": 6,
                            "sun": 0
                        };
                        //console.log(arrDays[dia]);
                        var existDay = $.inArray(arrDays[dia], diff);
                        //console.log(existDay);
                        if (existDay !== -1) {
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }

                    });
                });
                $('.fc-today-button').on('click', function() { // Para renderizar los campos validos cuando retroceda de mes
                    calendar.today();
                    $(".fc-day").each(function() {
                        if ($(this).hasClass("fc-day-past")) {
                            console.log("es pasado");
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }
                        var dia = $(this).attr("class").split(" ")[2].split("-")[2];
                        //console.log(dia);
                        var arrDays = {
                            "mon": 1,
                            "tue": 2,
                            "wed": 3,
                            "thu": 4,
                            "fri": 5,
                            "sat": 6,
                            "sun": 0
                        };
                        //console.log(arrDays[dia]);
                        var existDay = $.inArray(arrDays[dia], diff);
                        //console.log(existDay);
                        if (existDay !== -1) {
                            $(this).css("background-color", "tomato");
                            $(this).addClass("dis");
                        }

                    });
                });

            });

            function addDateToField(date, day) {
                $("#campos-fecha-hora").append('<input class="hora timepicker form-control" name="hora_reserva" id="hora_reserva" value="">');
                console.log(date);
                $("#date-reservation").val(date);
                var id_usuario = $("#id_profesional").val();
                console.log("llego aqui " + id_usuario);
                $.ajax({
                    url: "masters/calendario/get-enabletimes.php",
                    type: "POST",
                    data: {
                        id_dia: day,
                        id_profesional: id_usuario
                    },
                    success: function(response) {
                        let data = JSON.parse(response);
                        //console.log(data.nombre);

                        disable_all_times = [];


                        //console.log("#horas deshabilitadas");
                        //console.dir(disable_all_times);
                        disable_all_times = data.horas_deshabilitadas;

                        instanciar_timepicker_front(data.horas_deshabilitadas);
                        var time = $("#hora_reserva").pickatime("picker");
                        //time.render();
                        //console.dir(time.get());
                        //instanciar_timepicker_front();
                    },
                });
            }

            function getReservedTimes() {
               
                //console.log(date);
                var fecha = $("#date-reservation").val();
                var id_usuario = $("#id_profesional").val();
                console.log("llego aqui " + id_usuario);
                $.ajax({
                    url: "masters/calendario/get-reservedtimes.php",
                    type: "POST",
                    data: {
                        id_profesional: id_usuario,
                        fecha:fecha
                    },
                    success: function(response) {
                        let data = JSON.parse(response);
                        $(".picker__list-item").each(function(){
                            //console.log($(this).attr("aria-label"));
                            if($.inArray($(this).attr("aria-label"),data.horas_reservadas) != -1){
                                console.log("existe: "+$(this).attr("aria-label"));
                                $(this).addClass("picker__list-item--disabled");
                            }
                        })
                        //data.horas_reservadas.indexOf()
                    },
                });
            }
        </script>
        <script>
            $(".btn-register").on("click", function() {
                registerUser();
            });

            function registerUser() {
                var nombre = $("#nombre").val();
                var apellido = $("#apellido").val();
                var rut = $("#rut").val();
                var correo = $("#correo").val();
                if (nombre !== "" && apellido !== "" && rut !== "" && correo !== "") {
                    $("#span-val-register").html("");
                    $("#span-val-register-success").html("");
                    $.ajax({
                        url: "masters/perfiles/agregar_cliente.php",
                        type: "POST",
                        data: {
                            nombre: nombre,
                            apellido: apellido,
                            correo: correo,
                            rut: rut
                        },
                        success: function(response) {
                            console.log(response);
                            if (response === "Ok") {
                                $("#nombre").val("");
                                $("#apellido").val("");
                                $("#rut").val("");
                                $("#correo").val("");
                                $("#span-val-register-success").html("Registro exitoso, revisa tu correo para confirmar tu usuario e inicia sesión.");
                            }
                            if (response === "EmailExist") {
                                $("#span-val-register").html("Correo existe.");
                            }
                            if (response === "No") {
                                $("#span-val-register").html("Ocurrió un error registrando, si el problema persiste contacte al administrador.");
                            }
                        },
                    });
                } else {
                    $("#span-val-register").html("Debe completar todos los campos.");
                }

            }
        </script>

    <?php

} else {
    header("Location: profesionales.php");
}

    ?>