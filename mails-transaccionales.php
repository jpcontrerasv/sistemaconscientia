<?php include 'header.php' ?>

<!--------------------------------- 01A --------------------------------------->

<p style="text-align: center;">[CUENTA CREADA - 01A]</p>

<table width="600" style="margin:0 auto 0 auto; border-collapse:collapse;background-color:#f2f6fc;" cellspacing="0" cellpadding="0">
    <!--table head-->
    <tr>
        <td width="600" height="100" style="background:#FFFFFF; text-align:center;">
            <img src="http://grupoconscientia.cl/backend/assets/img/logo.png" style="width: 200px;">
        </td>
    </tr>
    <!--table body-->
    <tr>
        <td width="600" height="100" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="100" height="250">
                        &nbsp;
                    </td>
                    <td width="400" height="250" style="font-family:Arial, Helvetica, sans-serif; color:#3c3c3e; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:23px; text-align:center;">
                        Te damos la bienvenida <strong>[Nombre]</strong>
                        <br>
                        Tu usuario es: <strong>correo@correo.com</strong>
                        <br>
                        Tu contraseña temporal es: <strong>1234hola</strong>
                        <br><br>
                        <a href="login.php" style="color: #fff;background-color:#0061f2; border-color: #0061f2;padding:10px;border-radius:5px;margin:20px;">Ingresar</a>
                    </td>
                    <td width="100" height="250">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--table footer-->
    <tr>
        <td width="600" height="50" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#69707a; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:12px; text-align:center; background-color:#FFF;">
                        Grupo Conscientia 2021
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<hr>


<!--------------------------------- 02A --------------------------------------->

<p style="text-align: center;">[RECUPERAR CONTRASEÑA - 02A]</p>

<table width="600" style="margin:0 auto 0 auto; border-collapse:collapse;background-color:#f2f6fc;" cellspacing="0" cellpadding="0">
    <!--table head-->
    <tr>
        <td width="600" height="100" style="background:#FFFFFF; text-align:center;">
            <img src="http://grupoconscientia.cl/backend/assets/img/logo.png" style="width: 200px;">
        </td>
    </tr>
    <!--table body-->
    <tr>
        <td width="600" height="100" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="250">
                        &nbsp;
                    </td>
                    <td width="400" height="250" style="font-family:Arial, Helvetica, sans-serif; color:#3c3c3e; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:23px; text-align:center;">
                        <strong>Recupera tu contraseña</strong>
                        <br>
                        Has solicitado un cambio de contraseña.</strong>
                        <br><br>
                        <a href="cambiar-contrasena.php" style="color: #fff;background-color:#0061f2; border-color: #0061f2;padding:10px;border-radius:5px;margin:20px;">Cambiar contraseña aquí</a>
                    </td>
                    <td width="200" height="250">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--table footer-->
    <tr>
        <td width="600" height="50" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#69707a; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:12px; text-align:center; background-color:#FFF;">
                        Grupo Conscientia 2021
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<br>
<hr>




<!--------------------------------- 03A --------------------------------------->

<p style="text-align: center;">[CUENTA ELIMINADA - 03A]</p>

<table width="600" style="margin:0 auto 0 auto; border-collapse:collapse;background-color:#f2f6fc;" cellspacing="0" cellpadding="0">
    <!--table head-->
    <tr>
        <td width="600" height="100" style="background:#FFFFFF; text-align:center;">
            <img src="http://grupoconscientia.cl/backend/assets/img/logo.png" style="width: 200px;">
        </td>
    </tr>
    <!--table body-->
    <tr>
        <td width="600" height="100" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="250">
                        &nbsp;
                    </td>
                    <td width="400" height="250" style="font-family:Arial, Helvetica, sans-serif; color:#3c3c3e; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:23px; text-align:center;">
                        <strong>Tu cuenta ha sido eliminada</strong>
                        <br>
                        Por el siguiente motivo: <strong>[Motivo elminación]</strong>.</strong>
                        <br><br>
                        Si esto es un error, favor comunicarse a <a href="mailto:contacto@grupoconscientia.cl">contacto@grupoconscientia.cl</a>.
                    </td>
                    <td width="200" height="250">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--table footer-->
    <tr>
        <td width="600" height="50" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#69707a; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:12px; text-align:center; background-color:#FFF;">
                        Grupo Conscientia 2021
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br>
<hr>





<!--------------------------------- 04A --------------------------------------->

<p style="text-align: center;">[CONSULTA CANCELADA - 04A]</p>

<table width="600" style="margin:0 auto 0 auto; border-collapse:collapse;background-color:#f2f6fc;" cellspacing="0" cellpadding="0">
    <!--table head-->
    <tr>
        <td width="600" height="100" style="background:#FFFFFF; text-align:center;">
            <img src="http://grupoconscientia.cl/backend/assets/img/logo.png" style="width: 200px;">
        </td>
    </tr>
    <!--table body-->
    <tr>
        <td width="600" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="450">
                        &nbsp;
                    </td>
                    <td width="400" height="450" style="font-family:Arial, Helvetica, sans-serif; color:#3c3c3e; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:23px; text-align:center;">
                        <strong>Has cancelado la hora:</strong>
                        <br><br>
                        <ul style="text-align: left;">
                            <li><strong>ID</strong> 03321</li>
                            <li><strong>Profesional</strong> [Nombre profesional] [Apellido Profesional]</li>
                            <li><strong>Fecha y Hora</strong> [Día en números]/[Mes en palabras]/[año] - [Hora]</li>
                            <li><strong>Total pagado</strong> $15.000</li>
                        </ul>
                        <br><br>
                        <p style="background-color: #ccf5f7; color:#000; padding:5px;">No contempla reembolso</p>
                        Si esto es un error, favor comunicarse a <a href="mailto:contacto@grupoconscientia.cl">contacto@grupoconscientia.cl</a>.
                    </td>
                    <td width="200" height="450">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!--table footer-->
    <tr>
        <td width="600" height="50" style="text-align:center;">
            <table width="600" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="200" height="50" style="font-family:Arial, Helvetica, sans-serif; color:#69707a; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:12px; text-align:center; background-color:#FFF;">
                        Grupo Conscientia 2021
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



<?php include 'footer.php' ?>