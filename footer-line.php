<footer class="footer mt-auto footer-light">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 small text-center">Grupo Conscientia <?php echo date("Y"); ?> | <a href="terminos-y-condiciones.php">Terminos y Condiciones</a> | <a href="privacidad.php">Privacidad</a> </div>
        </div>
    </div>
</footer>