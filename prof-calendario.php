<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";

$db = connectToDB($dbData);

$stmt_esp = $db->prepare(selectCategoriaProfesional());
$stmt_esp->execute(array($_SESSION["id_usuario"]));
$result_categoria = $stmt_esp->fetchAll();

$meses = [
    "01" => "enero",
    "02" => "febrero",
    "03" => "marzo",
    "04" => "abril",
    "05" => "mayo",
    "06" => "junio",
    "07" => "julio",
    "08" => "agosto",
    "09" => "septiembre",
    "10" => "octubre",
    "11" => "noviembre",
    "12" => "diciembre",
];


$fecha = date('Y-m-d');

$stmta = $db->prepare(selectReservaProfesional());
$stmta->execute(array($_SESSION["id_usuario"], $fecha));
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);
$cant_results = count($result);



include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-dark bg-gradient-primary-to-secondary mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="calendar"></i></div>
                                        Mi calendario
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!--Informacion personal-->
                            <div id="infhoras">
                                <div class="card mb-4">
                                    <div class="card-header">Configurar mi horario</div>
                                    <?php if (isset($_GET["m"]) && $_GET["m"] == 1) { ?>
                                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            Día configurado correctamente
                                        </div>

                                    <?php } ?>



                                    <div class="card-body mb-4">
                                        <div class="sbp-preview">
                                            <div class="container sbp-preview-content">
                                                <div class="row">

                                                    <div class="col" id="cont-1">
                                                        <button id="1" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Lunes</button>
                                                    </div>
                                                    <div class="col" id="cont-2">
                                                        <button id="2" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Martes</button>
                                                    </div>
                                                    <div class="col" id="cont-3">
                                                        <button id="3" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Miércoles</button>
                                                    </div>
                                                    <div class="col" id="cont-4">
                                                        <button id="4" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Jueves</button>
                                                    </div>
                                                    <div class="col" id="cont-5">
                                                        <button id="5" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Viernes</button>
                                                    </div>
                                                    <div class="col" id="cont-6">
                                                        <button id="6" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Sábado</button>
                                                    </div>
                                                    <div class="col" id="cont-7">
                                                        <button id="7" class="btn btn-primary btn-xs cal-dia" type="button" data-fancybox data-src="#cancel03" data-modal="true" href="javascript:;">Domingo</button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="display: none;max-width:100vw;" id="cancel03" class=" card rounded-sm">
                                <form action="masters/calendario/form-configuracion.php" method="POST">
                                    <h2>Configurar horas (<span id="dia-de-semana"></span>)</h2>
                                    <input type="text" name="id_dia" id="id_dia" value="0" hidden>
                                    <div class="form-group">
                                        <br>
                                        <table id="tabla_servicio_0" class="table table-center">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>Hora de inicio</td>
                                                    <td>Hora fin</td>
                                                    <td>Opciones</td>
                                                </tr>
                                            </thead>
                                            <tbody id="body-variacion">

                                            </tbody>
                                        </table>
                                        <i id="add_0" class="add fa fa-plus" style="padding:3px;color:white;background-color:#2196f3;border-radius:2px;cursor:pointer;font-size:1.5em;"></i>
                                        <input value="1" name="filas_serv_0" id="filas_serv_0" hidden>
                                        <br>
                                        <label for="" id="vali_horas" style="font-weight: bold; font-size: .8em;color:red;"></label>

                                    </div>
                                    <div class="form-group d-flex justify-content-between w-100 ">
                                        <button data-fancybox-close class="btn btn-link btn-sm">Cancelar</button>
                                        <button class="btn btn-primary btn-lg save-day" type="submit">Guardar configuración</button>
                                    </div>
                                </form>


                            </div>

                            <!--Informacion personal-->
                            <div id="prox-horas">
                                <div class="card mb-4">
                                    <div class="card-header">Próximas horas</div>
                                    <div class="card-body">
                                        <div class="datatable">
                                            <table class="table table-bordered table-hover" id="tableReportes" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre Paciente</th>
                                                        <th>RUT Paciente</th>
                                                        <th>Mes Consulta</th>
                                                        <th>Día Consulta</th>
                                                        <th>Hora Consulta</th>
                                                        <th>Previsión Paciente</th>
                                                        <th>Total Pagado</th>
                                                        <th>Cancelar Hora</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th>Nombre Paciente</th>
                                                        <th>RUT Paciente</th>
                                                        <th>Mes Consulta</th>
                                                        <th>Día Consulta</th>
                                                        <th>Hora Consulta</th>
                                                        <th>Previsión Paciente</th>
                                                        <th>Total Pagado</th>
                                                        <th>Cancelar Hora</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                <?php foreach ($result as $res) {  ?>
                                                    <tr>
                                                        <td><?php echo utf8_encode($res["tx_nombre"].' '.$res["tx_apellido"]); ?></td>
                                                        <td><?php echo ($res["tx_rut"]); ?></td>
                                                        <td><?php   $mes = explode("-", $res["fecha_reserva"])[1]; echo $meses[$mes]; ?></td>
                                                            <td><?php $dia = explode("-", $res["fecha_reserva"])[2]; echo $dia; ?></td>
                                                            <td><?php echo ($res["hora_inicio"].' a '.$res["hora_fin_visual"]); ?></td>
                                                            <td><?php echo utf8_encode($res["nb_categoria"]); ?></td>
                                                            <td><?php echo ($res["total_consulta"]); ?></td>
                                                        <td>
                                                            <button class="btn btn-danger btn-xs" type="button" data-fancybox data-src="#cancel01" data-modal="true" href="javascript:;">Cancelar hora</button>
                                                            <div style="display: none;max-width:100vw;" id="cancel01" class=" card rounded-sm">
                                                                <h2>Cancelar hora</h2>

                                                                <form>
                                                                    <div class="form-group">
                                                                        <p>Se enviará un correo notificando al paciente notificando el cancelamiento de la hora.</p>
                                                                        <p>El paciente contará con un código para hacer un descuento equivalente para una próxima hora.</p>
                                                                        <p><strong>Esta acción es irreversible</strong>.</p>
                                                                    </div>
                                                                    <div class="form-group d-flex justify-content-between w-100 ">
                                                                        <button data-fancybox-close class="btn btn-link btn-sm">Cerra ventana</button>
                                                                        <button data-fancybox-close class="btn btn-danger btn-lg">Cancelar hora</button>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php }  ?>
                                                    

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>

    <script src="js/calendario-conf.js"></script>

    
    