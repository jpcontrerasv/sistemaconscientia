<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);
?>
<?php include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content">
                            <div class="row align-items-center justify-content-between pt-3">
                                <div class="col-auto mb-3">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="edit-2"></i></div>
                                        Crear Perfil
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                            <!--Informacion personal-->
                            <div id="infpersonal">
                                <!--Información de la tienda-->
                                <div class="card mb-4 shadow-none">
                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <p>Utiliza este formulario para agregar un nuevo perfil a la plataforma. Este nuevo usuario será notificado al correo indicado.</p>
                                                <form id="formPerfil">
                                                    <div class="form-group row">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Nombres</strong></label>
                                                            <input class="form-control" value="" name="nombre" type="text" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Apellidos</strong></label>
                                                            <input class="form-control" value="" name="apellido" type="text" required />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>RUT</strong></label>
                                                            <input id="rut" class="form-control" value="" name="rut" type="text" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Correo Electrónico</strong></label>
                                                            <input class="form-control" value="" name="correo" type="email" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Teléfono</strong></label>
                                                            <input class="form-control form-perfil" value="" type="tel" name="telf" id="telf"  required />
                                                        </div>

                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Registro del MINSAL</strong></label>
                                                            <input class="form-control" name="minsal" id="minsal" value="" type="text" required />
                                                        </div>
                                                    </div>

                                                    <button type="submit" class="btn btn-primary mb-2">Crear</button>
                                                    <br>
                                                    <small class="text-muted">[En caso de éxito: mail <a href="mails-transaccionales.php">01A</a>]</small>

                                                    

                                                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                                                        El mail ingresado ya existe
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="alert alert-danger alert-dismissible fade show mt-2" role="alert">
                                                        El RUT ingresado ya existe
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                        Ingreso exitoso. El usuario será notificado mediante el correo indicado.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>


                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>