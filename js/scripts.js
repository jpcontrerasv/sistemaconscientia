/*!
    * Start Bootstrap - SB Admin Pro v1.3.0 (https://shop.startbootstrap.com/product/sb-admin-pro)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under SEE_LICENSE (https://github.com/StartBootstrap/sb-admin-pro/blob/master/LICENSE)
    */

(function ($) {

    "use strict";


    // Enable Bootstrap tooltips via data-attributes globally
    $('[data-toggle="tooltip"]').tooltip();

    // Enable Bootstrap popovers via data-attributes globally
    $('[data-toggle="popover"]').popover();

    $(".popover-dismiss").popover({
        trigger: "focus"
    });

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $("#layoutSidenav_nav .sidenav a.nav-link").each(function () {
        if (this.href === path) {
            $(this).addClass("active");
        }
    });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function (e) {
        e.preventDefault();
        $("body").toggleClass("sidenav-toggled");
    });

    // Activate Feather icons
    feather.replace();

    // Activate Bootstrap scrollspy for the sticky nav component
    $("body").scrollspy({
        target: "#stickyNav",
        offset: 82
    });


    //RUT
    $("input#rut").rut({
        formatOn: 'keyup', validateOn: 'keyup'
    }).on('rutInvalido', function (e) {
        $("#al-for-rut").removeClass('d-none');
        $("#rut").removeClass('is-valid');
        $("#rut").addClass('is-invalid');
        //alert("El rut " + $(this).val() + " es inválido");
    }).on('rutValido', function (e) {
        $("#al-for-rut").addClass('d-none');
        $("#rut").addClass('is-valid');
        $("#rut").removeClass('is-invalid');
    });

    // Scrolls to an offset anchor when a sticky nav link is clicked
    $('.nav-sticky a.nav-link[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate({
                    scrollTop: target.offset().top - 81
                },
                    200
                );
                return false;
            }
        }
    });

    // Click to collapse responsive sidebar
    $("#layoutSidenav_content").click(function () {
        const BOOTSTRAP_LG_WIDTH = 992;
        if (window.innerWidth >= 992) {
            return;
        }
        if ($("body").hasClass("sidenav-toggled")) {
            $("body").toggleClass("sidenav-toggled");
        }
    });

    // Init sidebar
    let activatedPath = window.location.pathname.match(/([\w-]+\.html)/, '$1');

    if (activatedPath) {
        activatedPath = activatedPath[0];
    } else {
        activatedPath = 'index.html';
    }

    let targetAnchor = $('[href="' + activatedPath + '"]');
    let collapseAncestors = targetAnchor.parents('.collapse');

    targetAnchor.addClass('active');

    collapseAncestors.each(function () {
        $(this).addClass('show');
        $('[data-target="#' + this.id + '"]').removeClass('collapsed');

    })

    //Adicional



    //Remember password
    $(function () {

        if (localStorage.chkbx && localStorage.chkbx != '') {
            $('#remember_me').attr('checked', 'checked');
            $('#xip_Name').val(localStorage.usrname);
            $('#xip_Password').val(localStorage.pass);
        } else {
            $('#remember_me').removeAttr('checked');
            $('#xip_Name').val('');
            $('#xip_Password').val('');
        }

        $('#remember_me').click(function () {

            if ($('#remember_me').is(':checked')) {
                // save username and password
                localStorage.usrname = $('#xip_Name').val();
                localStorage.pass = $('#xip_Password').val();
                localStorage.chkbx = $('#remember_me').val();
            } else {
                localStorage.usrname = '';
                localStorage.pass = '';
                localStorage.chkbx = '';
            }
        });
    });

    //Masonry
    var $container = $('#gridmason');
    $container.imagesLoaded(function () {
        $container.masonry({
            itemSelector: '.itemmaso'
        });
    });
    //Fancybox
    $('[data-fancybox="gallery"]').fancybox({
        infobar: false,
    });

    //Modal abierto
    //$("#modalCarrito").modal('show');



})(jQuery);

//Datatable modal carrito
$(document).ready(function () {

    $('button#crearCuenta').attr("disabled", true);
    $("#term_condiciones").prop("checked", false);
    $("#term_condiciones").on("change", function () {
        if ($(this).is(':checked')) {
            // Hacer algo si el checkbox ha sido seleccionado
            $('button#crearCuenta').attr("disabled", false);
        } else {
            // Hacer algo si el checkbox ha sido deseleccionado
            $('button#crearCuenta').attr("disabled", true);
        }


    })
    //Volver atrás 
    function goBack() {
        window.history.back();
    }

    //Table carrito
    $('#tableCarrito').DataTable({
        paging: false,
        compact: true,
        bFilter: false,
        bInfo: false,
        autoWidth: true,
        "language": {
            "emptyTable": "No tienes reservas"
        }
    });
    var table = $('#tableCarrito').DataTable();
    $('#tableCarrito tbody').on('click', 'button.borrar', function () {
        table
            .row($(this).parents('tr'))
            .remove()
            .draw();
    });


    //Table reportes
    $('#tableReportes').DataTable({
        dom: 'Bfrtip',
        buttons: ['excel']
    });

    //Table reembolsos
    $('#tableRH').DataTable({
        dom: 'Bfrtip',
        buttons: ['excel']
    });

    //Toggle option cancelar horas
    $('[class^=is]').hide();
    $("#modifHora").change(function () {
        var value = $("#modifHora option:selected").val();
        var theDiv = $(".is" + value);

        theDiv.slideDown();
        theDiv.siblings('[class^=is]').slideUp();
    });

    //Mostrar - Ocultar password
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });

    //Mostrar - Ocultar password
    $("#show_hide_password_b a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password_b input').attr("type") == "text"){
            $('#show_hide_password_b input').attr('type', 'password');
            $('#show_hide_password_b i').addClass( "fa-eye-slash" );
            $('#show_hide_password_b i').removeClass( "fa-eye" );
        }else if($('#show_hide_password_b input').attr("type") == "password"){
            $('#show_hide_password_b input').attr('type', 'text');
            $('#show_hide_password_b i').removeClass( "fa-eye-slash" );
            $('#show_hide_password_b i').addClass( "fa-eye" );
        }
    });


    //Textarea counter
    var maxLength = 1000;
    $('textarea#textareaProf').keyup(function () {
        var length = $(this).val().length;
        var length = maxLength - length;
        $('#chars').text(length);
    });

    //Sticky 
    $("#topnav-home").stick_in_parent();

    //Lista de categorías Profesional
    $('#addPro').click(function () {
        var listvaluePro = $('#textboxPro').val();
        if (listvaluePro.length == 0) {
            Swal.fire({
                title: '',
                html: '<p>Debe ingresar un tipo de profesional</p>',
            })
        }
        else {
            //$('ul#listPro').append('<li class="list-group-item d-flex justify-content-between align-items-center">' + listvaluePro + '<span class="badge badge-danger badge-pill">&nbsp;</span></li>');
            //$('#textboxPro').val("");
        }
    });
    /*$('#listPro').on('click', 'li', function (e) {
        var liTextPro = $(this).text();
        if (confirm("¿Desea remover la categoría " + liTextPro + "?")) {
            $(this).remove();
        } else {
        }
    }); */


    //Lista de categorías Previsiones
    $('#addPrev').click(function () {
        var listvaluePrev = $('#textboxPrev').val();
        if (listvaluePrev.length == 0) {
            Swal.fire({
                title: '',
                html: '<p>Debe ingresar un tipo de previsión</p>',
            })
        }
        else {
            //$('ul#listPrev').append('<li class="list-group-item d-flex justify-content-between align-items-center">' + listvaluePrev + '<span class="badge badge-danger badge-pill">&nbsp;</span></li>');
            //$('#textboxPrev').val("");
        }
    });
    /*$('#listPrev').on('click', 'li', function (e) {
        var liTextPrev = $(this).text();
        if (confirm("¿Desea remover la categoría " + liTextPrev + "?")) {
            $(this).remove();
        } else {
        }
    }); */

    //Lista de categorías Motivos de consulta
    $('#addCons').click(function () {
        var listvalueCons = $('#textboxCons').val();
        if (listvalueCons.length == 0) {
            Swal.fire({
                title: '',
                html: '<p>Debe ingresar un motivo de consulta</p>',
            })
        }
        else {
            //$('ul#listCons').append('<li class="list-group-item d-flex justify-content-between align-items-center">' + listvalueCons + '<span class="badge badge-danger badge-pill">&nbsp;</span></li>');
            //$('#textboxCons').val("");
        }
    });
    /*$('#listCons').on('click', 'li', function (e) {
        var liTextCons = $(this).text();
        if (confirm("¿Desea remover la categoría " + liTextCons + "?")) {
            $(this).remove();
        } else {
        }
    });*/

    //Tags profesionales
    var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
        removeItemButton: true
    });

    //Sticky 
    $("#topnav-home").stick_in_parent();

    $('#formPerfil').on('submit', function (e) {
        e.preventDefault()
        console.log({
            data: {
                nombre: $("[name=nombre]").val(),
                apellido: $("[name=apellido]").val(),
                rut: $("[name=rut]").val(),
                correo: $("[name=correo]").val(),
                telefono: $("[name=telf]").val(),
                minsal: $("[name=minsal]").val()
            }
        })
        $.ajax({
            url: "masters/perfiles/agregar_perfil.php",
            type: "POST",
            data: {
                nombre: $("[name=nombre]").val(),
                apellido: $("[name=apellido]").val(),
                rut: $("[name=rut]").val(),
                correo: $("[name=correo]").val(),
                telf: $("[name=telf]").val(),
                minsal: $("[name=minsal]").val()
            },
            success: function (response) {
                if (response === "No") {
                    alert("Ocurrió un error")
                } else {
                    alert("Registrado correctamente")
                    $("[name=nombre]").val("")
                    $("[name=apellido]").val("")
                    $("[name=rut]").val("")
                    $("[name=correo]").val("")
                    $("[name=telf]").val(""),
                    $("[name=minsal]").val("")
                }
            },
        });
    });


    //Procesa eliminación de perfil
    $('.cerrarCuentaButton').on('click', function (e) {
        const id = $(this).attr("usuario")
        const tdNombre = $(`tr[usuario=${id}]>td:first-child`).text();
        if (confirm("¿Desea remover el usuario " + tdNombre + "?")) {
            $.ajax({
                url: "masters/perfiles/eliminar_perfil.php",
                type: "POST",
                data: {
                    id,
                    motivo: $("form.cerrarCuenta[usuario=" + id + "] select.razonEliminar").val()
                },
                success: function (response) {
                    if (response === "No") {
                        alert("Ocurrió un error")
                    } else {
                        if (response === "Solo eliminado") {
                            alert("Se elimino pero no se pudo guardar el motivo")
                        } else {
                            alert("Eliminado correctamente")
                        }
                        $(`tr[usuario=${id}]`).remove()
                    }
                },
            });
            //$(this).remove();
        } else {
        }
    });



    //Procesa Formulario de información profesional "Categoria de perfil"
    $("#formInformacionProfesional").on('submit', function (e) {
        e.preventDefault()
        const data = $(this).serializeArray().reduce(function (a, z) { a[z.name] = z.value; return a; }, {})
        $.ajax({
            url: "masters/perfiles/actualizar_categoria_perfil.php",
            type: "POST",
            data: data,
            success: function (response) {

                if (response === "No") {
                    alert("Ocurrió un error")
                } else {
                    alert("Actualizados correctamente")
                }

            },
        })
    });

    //Coloca la imagen en el img
    $("[name=perfil]").on('change', function (e) {

        new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL($(this)[0].files[0]);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        }).then((imagen) => {
            $("#imagen").attr("src", imagen).css("display", "initial")
        })
    });


    //Procesa Formulario de información personal
    $("#formInformacionPersonal").on('submit', function (e) {

        e.preventDefault()
        console.log({ e })
        const data = new FormData($(this)[0])
        $.ajax({
            url: "masters/perfiles/actualizar_perfil.php",
            type: "POST",
            data: data,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function (response) {
                console.log(response)
                if (response === "No") {
                    alert("Ocurrió un error")
                } else {
                    if (response === "Solo informacion") {
                        alert("Solo se actualizaron los datos, no se subio imagen")
                    } else {
                        alert("Actualizados correctamente")
                    }

                }

            },
        })
    })


});
/*

function agregar_registro_categoria(data) {
    $.ajax({
        url: "masters/categorias/agregar_categoria.php",
        type: "POST",
        data: data ,
        success: function (response) {
            console.log({data})
            if(response === "No"){
                alert("Ocurrió un error")
            }else{

                if(data.id_tipo_categoria === 1){
                    $("#listPro").append(`
                        <li categoria="${response}" class="list-group-item d-flex justify-content-between align-items-center">${data.nombre}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
                    `);
                }else if(data.id_tipo_categoria === 2){
                    $("#listPrev").append(`
                        <li categoria="${response}" class="list-group-item d-flex justify-content-between align-items-center">${data.nombre}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
                    `);
                }else if(data.id_tipo_categoria === 3){
                    $("#listCons").append(`
                        <li categoria="${response}" class="list-group-item d-flex justify-content-between align-items-center">${data.nombre}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
                    `);
                }
            }
            // let template = "";
            // tasks.forEach((task) => {
            //  template += `
            //             <li class="list-group-item d-flex justify-content-between align-items-center">${task.nb_categoria}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
            //     `;
            // });
            // $("#listPro").html(template);
        },
    });
}
function eliminar_registro_categoria(id) {
    $.ajax({
        url: "masters/categorias/eliminar_categoria.php",
        type: "POST",
        data: {
            id
        } ,
        success: function (response) {
            if(response === "Si"){
                $(`[categoria=${id}]`).remove()
            }else{
                alert("Ocurrió un error")
            }
            // let template = "";
            // tasks.forEach((task) => {
            //  template += `
            //             <li class="list-group-item d-flex justify-content-between align-items-center">${task.nb_categoria}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
            //     `;
            // });
            // $("#listPro").html(template);
        },
    });
}
*/