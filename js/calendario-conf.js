$(document).ready(function () {
    var horas_inhabilitadas = []; //{from: [13,30], to: [16,00]}

    var timepicker;
    var timepicker2;

    

    function instanciar_timepicker() {
        console.log("instanciar timepicker");
        console.dir(horas_inhabilitadas);
        $('.timepicker').each(function () {
            $(this).pickatime({
                format: 'HH:i',
                disable: horas_inhabilitadas,

            });

        });
    }

    $(document).on('click', '.hora', function () {

        var rowCount = $('#tabla_servicio_0 tbody>tr').length;

        //var vacio = recorrerCampos(rowCount);

        var id = $(this).attr("id");

        setTimePicker(id, horas_inhabilitadas);

    });

    function setTimePicker(id, times_to_disable) {
        $('input.timepicker').each(function () {

            var time = $(this).pickatime("picker");

            //var time = timepicker.pickatime("picker");
            time.set('disable', times_to_disable);
            //console.dir(time.get('enable'));

        });

    }


    function recorrerCampos(filas) {
        var vacio = false;

        for (var i = 1; i <= filas; i++) {
            
            var hora_inicio = document.getElementById("0_hora_inicio-" + i).value;
            var hora_fin = document.getElementById("0_hora_fin-" + i).value;

            //console.log("valor hora inicio de fila " + i + ": " + document.getElementById("0_hora_inicio-" + i).value);
            //console.log("valor hora final de fila " + i + ": " + document.getElementById("0_hora_fin-" + i).value);

            if (hora_inicio === "" || hora_fin === "") {
                console.log("detecta vacío");
                vacio = true;
            }

        }

        return vacio;
    }

    function inhabilitarHoras(filas) {
        var horas = {};
        //horas_inhabilitadas = [];
        console.log(filas);
        console.log("Inicio recorrer campos #### Horas: ");
        console.dir(horas_inhabilitadas);
        console.log("Fin salida de horas_inhabilitadas #### ");
        var hora_inicio_format = [];
        var hora_fin_format = [];

        for (var i = 1; i <= filas; i++) {
            hora_inicio_format = [];
            hora_fin_format = [];
            var hora_inicio = document.getElementById("0_hora_inicio-" + i).value;
            var hora_fin = document.getElementById("0_hora_fin-" + i).value;

            //console.log("valor hora inicio de fila " + i + ": " + document.getElementById("0_hora_inicio-" + i).value);
            //console.log("valor hora final de fila " + i + ": " + document.getElementById("0_hora_fin-" + i).value);

            if (hora_inicio != "" && hora_fin != "") {
                //console.log("##### Hora inicio format valores posicion: " + i);
                //console.dir(hora_inicio_format);
                hora_inicio_format.push(parseInt(hora_inicio.split(":")[0]));
                hora_inicio_format.push(parseInt(hora_inicio.split(":")[1]));
                hora_fin_format.push(parseInt(hora_fin.split(":")[0]));
                hora_fin_format.push(parseInt(hora_fin.split(":")[1]));


                horas["from"] = hora_inicio_format;
                horas["to"] = hora_fin_format;

                if(horas_inhabilitadas.length !== 0){
                    if(i === filas){
                    $.each(horas_inhabilitadas, function(i, item) {
                        //console.dir(item.from);
                        if((item.from[0] === parseInt(hora_inicio.split(":")[0]) && item.from[1] === parseInt(hora_inicio.split(":")[1])) && 
                        (item.to[0] === parseInt(hora_fin.split(":")[0]) && item.to[1] === parseInt(hora_fin.split(":")[1]))                   
                        ){
                            console.log("existe ya en el array");
                            return true;
                        }else{
                            //console.log("no existe");
                            
                            horas_inhabilitadas.push(horas);
                            return false;
                        }
                        
                    });
                }
                }else{
                    horas_inhabilitadas.push(horas);
                }
                
                
                //console.dir(horas);

            }


        }

        return horas_inhabilitadas;
    }



    	$(document).on("click",".cal-dia",function(){
            var id_dia = $(this).attr("id");
            $("#id_dia").val(id_dia);
            $("#body-variacion").html("");
            $.ajax({
                url: "masters/calendario/form-horarios.php",
                type: "POST",
                data: { 
                    id_dia:id_dia
                 },
                success: function (response) {
                     let data = JSON.parse(response);
                     //console.log(data.nombre);
                     
                     console.log("#horas deshabilitadas");
                     console.dir(data.horas_deshabilitadas);
                     horas_inhabilitadas = data.horas_deshabilitadas;
                     $("#body-variacion").html(data.formulario);
                     $("#dia-de-semana").html(data.dia);
                     $("#filas_serv_0").val(data.cant_filas);
                     instanciar_timepicker();
                	
                },
            });
        }); 

    /* añadir y eliminar tablas dinámicamente*/
    $(document).on('click', '.add', function () {
        var id = $(this).attr('id').split("_")[1];
        console.log("al darle add: "+id);
        //alert(id);
        //console.dir($('#tabla_servicio_0 tbody>tr'));
        var rowCount = $('#tabla_servicio_0 tbody>tr').length;
        console.log(rowCount);
        var vacio = recorrerCampos(rowCount);

        console.log("esta vacío: " + vacio);
        //console.dir(vacio);
        if (vacio) {
            $("#vali_horas").html("Para agregar debe completar las filas de la tabla");
        } else {
            $("#vali_horas").html("");
            //console.dir($("#tabla_servicio_" + id));
            addTableRow($("#tabla_servicio_" + id), id);
            inhabilitarHoras(rowCount);
            instanciar_timepicker();
            // prevent button redirecting to new page
            return false;
        }
        // add new row to table using addTableRow function

    });

    // function to add a new row to a table by cloning the last row and 
    // incrementing the name and id values by 1 to make them unique
    function addTableRow(table, id) {
        //console.log("id de tabla :" +table.attr("id"));
        
        var id_tabla = table.attr("id");
        var parts;
        var cant_periodos = parseInt($("#filas_serv_" + id).val());

        // clone the last row in the table
        var $tr = $(table).find("tbody tr:last").clone('');
        // get the name attribute for the input and select fields
        $tr.find("input,select").attr("name", function () {
            // break the field name and it's number into two parts
            parts = this.id.split("-");
            // create a unique name for the new field by incrementing
            // the number for the previous field by 1
            return parts[0] + "-" + ++parts[1];

            // repeat for id attributes
        }).attr("id", function () {
            parts = this.id.split("-");
            return parts[0] + "-" + ++parts[1];
        });
        // append the new row to the table
        $(table).find("tbody tr:last").after($tr[($tr.length - 1)]);
        var a = $(table).find("tbody tr:last");
        a.find("td>input").each(function () {
            $(this).val("");
        });
        $tr.hide().fadeIn('slow');

        //document.getElementById(id + "_hora_inicio-" + parts[1]).value = "";
        //document.getElementById(id + "_hora_fin-" + parts[1]).value = "";
        $("#"+id+"_hora_inicio-"+ parts[1]).val("");
        $("#"+id+"_hora_fin-"+ parts[1]).val("");
  
        $("#filas_serv_" + id).val(parts[1]);
        var rowCount = 0;
        $("#" + id_tabla + " tbody tr td:first-child").text(function () {
            return ++rowCount;
        });
        //instanciar_timepicker();

    }
    var rowCount = 0;
    $(document).on("click", ".remove_button", function () {
        
        //alert($(this).closest("tr").index());
        var indice_tabla = $(this).closest("tr").index();

        if (indice_tabla === 0) {

        } else {
            //detectar id de tabla para saber a cual eliminar y validar que no esté menos de 1
            var tabla = $("#" + $(this).closest('table').attr('id'));
            var tabla_id = tabla.attr('id');
            console.dir(tabla_id);
            var id_servicio = tabla_id.split("_")[2];

            if ($('#' + tabla_id + ' tbody tr').length === 1)
                return;
            $(this).parents("tr").fadeOut('slow', function () {
                // Iba a obtener las horas que estoy borrando para habilitarlas

                $(this).remove();
                //// ***********************************

                var long = $('#' + tabla_id + ' tbody tr').length

                
                for (var m = 1; m < long; m++) {
                    console.log("M: " + m);
                    var $tr = $('#' + tabla_id).find("tbody tr:eq(" + m + ")");
                    //console.dir($tr);
                    $tr.find("input,select").attr("name", function () {
                        // break the field name and it's number into two parts
                        var parts = this.id.split("-");

                        // create a unique name for the new field by incrementing
                        // the number for the previous field by 1

                        return parts[0] + "-" + (m + 1);

                        // repeat for id attributes
                    }).attr("id", function () {
                        var parts = this.id.split("-");
                        console.log("retorno: " + parts[0] + "-" + (m + 1));
                        return parts[0] + "-" + (m + 1);
                    });
                }


                rowCount = 0;

                var cant_periodos = (parseInt($("#filas_serv_" + id_servicio).val()) - 1);
                $("#filas_serv_" + id_servicio).val(cant_periodos);
                //console.log("valor de filas_ser:"+$("#filas_serv_"+id_servicio).val());
                $("#" + tabla_id + " tbody tr td:first-child").text(function () {
                    //alert(cant_periodos);
                    return ++rowCount;

                });

            });

        }

    });

    /* fin de añadir y eliminar*/




});


