<?php include 'header.php' ?>
<?php include 'topnav-front.php' ?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-5 text-left">
                <h1 class="rock-font display-4 color-granate">Términos y Condiciones</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card p-4 mb-5">
                    <div class="card-header mb-3 d-flex justify-content-end p-2">
                        

                        <p class="text-muted m-0"><small>Última actualización <?php echo date("d M Y"); ?> <?php echo "" . date("H:i"); ?></small>
                    </div>

                    <div class="card-body">
                        <p>
                            Los siguientes son los Términos y Condiciones (o "Acuerdo") que rigen su acceso y uso de nuestra plataforma en línea a través del cual se puede proporcionar asesoramiento (colectivamente, la "Plataforma"). La plataforma se puede acceder desde el sitio web grupoconscientia.cl y sus aplicaciones.
                            Al acceder o utilizar la Plataforma, está aceptando este Acuerdo. Debe leer este Acuerdo detenidamente antes de comenzar a usar la Plataforma. Si no acepta estar sujeto a ningún término de este Acuerdo, no debe acceder a la Plataforma.
                            Cuando los términos "nosotros", "nos", "nuestro" o similares se usan en este Acuerdo, se refieren a cualquier compañía que posee y opera la Plataforma (la "Compañía").
                        </p>
                        <h4> PRIMERA.- Los profesionales de la salud </h4>
                        <p>
                            La Plataforma puede usarse para conectarlo con un profesional de la salud que le proporcionará servicios a través de la Plataforma.
                            Requerimos que cada Profesional que brinde Servicios de salud en la Plataforma sea un profesional acreditado por cualquier casa de estudio reconocida por el estado de Chile. Debe tener experiencia acreditada en el área de su expertise así como de especializaciones reconocidas por el estado ó programas de posgrados.
                            Los Profesionals deben tener un título profesional y al menos 3 años de experiencia o al menos 2,000 horas de experiencia práctica (CONAPC), y deben estar calificados y certificados por su respectivo consejo profesional después de completar con éxito la educación, los exámenes y la capacitación necesarios y requisitos de práctica según corresponda.
                            Los Profesionals son proveedores independientes que no son nuestros empleados ni agentes ni representantes. El rol de la Plataforma se limita a habilitar los Servicios de Profesional, mientras que los Servicios de Profesional son responsabilidad del Profesional que los proporciona. Si considera que los Servicios del Profesional proporcionados por el Profesional no se ajustan a sus necesidades o expectativas, puede cambiar a un Profesional diferente que brinde servicios a través de la Plataforma.
                        </p>
                        <p>
                            Si bien esperamos que los Servicios de Psicología sean beneficiosos para usted, usted comprende, acepta y reconoce que pueden no ser la solución adecuada para las necesidades de todos y que pueden no ser apropiados para cada situación particular y / o pueden no ser un sustituto completo de un Examen cara a cara y / o atención en cada situación particular.
                            SI ESTÁ PENSANDO EN EL SUICIDIO O SI ESTÁ CONSIDERANDO DAÑARSE A USTED MISMO O A OTROS, O SI SIENTE QUE CUALQUIER OTRA PERSONA PUEDE ESTAR EN ALGÚN PELIGRO O SI TIENE ALGUNA EMERGENCIA MÉDICA, DEBE LLAMAR INMEDIATAMENTE AL NÚMERO DE SERVICIO DE EMERGENCIA (133 en Chile) Y NOTIFICAR A LAS AUTORIDADES PERTINENTES. LA PLATAFORMA NO ESTÁ DISEÑADA PARA USO EN NINGÚN CASO ANTERIOR Y LOS PROFESIONALES LISTADOS EN LA PLATAFORMA NO PUEDEN PROPORCIONAR LA ASISTENCIA REQUERIDA EN NINGÚN CASO ANTERIOR.
                            LA PLATAFORMA NO ESTÁ DISEÑADA PARA PROPORCIONAR UN DIAGNÓSTICO CLÍNICO QUE REQUIERE UNA EVALUACIÓN EN PERSONA. LA PLATAFORMA NO REMPLAZA LA CONSULTA PRESENCIAL Y NO DEBE UTILIZARLA PARA RETRASAR EL BUSCAR UNA CONSULTA PRESENCIAL. LA PLATAFORMA NO ES ÚTIL SI NECESITA ALGUNA DOCUMENTACIÓN OFICIAL O APROBACIONES PARA PROPÓSITOS LEGALES U OTROS.
                        </p>
                        <h4> SEGUNDA.- Seguridad de los datos </h4>
                        <p>
                            AL ACEPTAR ESTE ACUERDO Y / O AL USAR LA PLATAFORMA, TAMBIÉN ACEPTA LOS TÉRMINOS DE LA POLÍTICA DE PRIVACIDAD. LA POLÍTICA DE PRIVACIDAD SE INCORPORA Y ES UNA PARTE DE ESTE ACUERDO. LAS MISMAS REGLAS QUE SE APLICAN CON RESPECTO A CAMBIOS Y REVISIONES DE ESTE ACUERDO TAMBIÉN SE APLICAN A CAMBIOS Y REVISIONES DE LA POLÍTICA DE PRIVACIDAD.
                            Proteger y salvaguardar cualquier información que proporcione a través de la Plataforma es extremadamente importante para nosotros. Puede encontrar información sobre nuestras prácticas de seguridad y privacidad en nuestra Política de privacidad disponible en grupoconscientia.cl/terms (La "Política de privacidad").
                        </p>

                        <h4> TERCERA.- Renuncia de garantía y limitación de responsabilidad </h4>
                        <p>
                            USTED NOS LIBERA Y ACEPTA QUE NOSOTROS NO SOMOS RESPONSABLES DE CUALQUIER ACCIÓN CAUSADA Y RECLAMACIONES DE CUALQUIER NATURALEZA RESULTANTE DE LOS SERVICIOS DEL Profesional EN LA PLATAFORMA, INCLUYENDO (SIN LIMITACIÓN) CUALQUIER ACTO, OMISIÓN, OPINIÓN, RESPUESTA, ASESORAMIENTO, SUGERENCIA, INFORMACIÓN Y / O SERVICIO DE CUALQUIER Profesional Y / O CUALQUIER OTRO CONTENIDO O INFORMACIÓN ACCESIBLE A TRAVÉS DE LA PLATAFORMA.
                            USTED ENTIENDE, ACEPTA Y RECONOCE QUE LA PLATAFORMA SE PROPORCIONA "TAL CUAL" SIN NINGUNA GARANTÍA EXPRESA O IMPLÍCITA DE NINGÚN TIPO, INCLUYENDO PERO SIN LIMITARSE A LA COMERCIABILIDAD, SEGURIDAD. EL USO DE LA PLATAFORMA ES BAJO SU PROPIO RIESGO. EN LA MEDIDA MÁS COMPLETA DE LA LEY, NEGAMOS EXPRESAMENTE TODAS LAS GARANTÍAS DE CUALQUIER TIPO, INCLUSO EXPRESAS O IMPLÍCITAS.
                            USTED ENTIENDE, ACEPTA Y RECONOCE QUE NO SEREMOS RESPONSABLES CON USTED O CON NINGÚN TERCERO POR CUALQUIER DAÑO INDIRECTO, INCIDENTAL, CONSECUENTE, ESPECIAL, PUNITIVO O EJEMPLAR.
                            USTED ENTIENDE, ACEPTA Y RECONOCE QUE NUESTRA RESPONSABILIDAD AGREGADA POR DAÑOS DERIVADOS CON RESPECTO A ESTE ACUERDO Y CUALQUIERA Y TODO EL USO DE LA PLATAFORMA NO EXCEDERÁ LA CANTIDAD TOTAL DE DINERO PAGADO POR USTED A TRAVÉS DE LA PLATAFORMA EN EL PERÍODO DE 3 MESES ANTERIORES AL PERÍODO LA RECLAMACIÓN.
                            Si la ley aplicable no permite la limitación de responsabilidad como se establece anteriormente, la limitación se considerará modificada únicamente en la medida necesaria para cumplir con la ley aplicable.
                            Esta sección (limitación de responsabilidad) sobrevivirá a la terminación o vencimiento de este Acuerdo.
                        </p>
                        <h4> CUARTA.- Su cuenta, representaciones, conducta y compromisos. </h4>
                        <p>
                            Por la presente, confirma que es mayor de dieciocho (18) años, o puede legalmente dar su consentimiento para recibir tratamiento, o que tiene el consentimiento de la madre, padre o tutor legal, y que legalmente puede celebrar un contrato. El consentimiento debe ser entregado verbalmente al profesional durante la primera consulta.
                            Usted acepta y confirma que se encuentra físicamente o es residente del Estado o País que ha elegido como su residencia actual al crear su cuenta.
                            Usted acepta proporcionar "Información de contacto" (su contacto personal y / o un familiar / pariente cercano) a su Profesional para que actúe como contacto en caso de una crisis de salud mental u otra emergencia.
                            Por la presente, confirma y acepta que toda la información que proporcionó en través de la Plataforma, y la información que proporcionará a través de la Plataforma en el futuro, es precisa, verdadera, actual y completa. Además, acepta que durante la vigencia de este Acuerdo se asegurará de mantener y actualizar esta información para que continúe siendo precisa, actual y completa.
                            Usted reconoce que su capacidad para acceder y utilizar el Servicio está condicionada a la veracidad de la información que proporcione con respecto a su edad, residencia e información de contacto y que los Profesionals a los que accede ("Proveedores") confían en esta certificación para interactuar con usted y proporcionar los Servicios.
                            Usted acepta y se compromete a no utilizar la cuenta o el Acceso a la cuenta de ninguna otra persona por ningún motivo.
                            Usted acepta, confirma y reconoce que es responsable de mantener la confidencialidad de su contraseña y cualquier otra información de seguridad relacionada con su cuenta (colectivamente "Acceso a la cuenta"). Le recomendamos que cambie su contraseña con frecuencia y que tenga especial cuidado en proteger su contraseña.
                            Usted acepta, confirma y reconoce que no seremos responsables de ninguna pérdida o daño ocasionado por el uso de su cuenta por parte de otra persona, ya sea con o sin su consentimiento o conocimiento
                            Usted acepta notificarnos de inmediato sobre cualquier uso no autorizado de su Acceso a la cuenta o cualquier otra inquietud por incumplimiento de la seguridad de su cuenta.
                        </p>
                        <p>
                            Usted acepta, confirma y reconoce que es el único responsable de todas las actividades realizadas en el Acceso a su cuenta. Además, reconoce y acepta que lo consideraremos responsable de cualquier daño o pérdida incurrido como resultado del Acceso a la cuenta por parte de cualquier persona autorizada o no, y acepta indemnizarnos por dicho daño o pérdida.
                            Usted acepta y confirma que su uso de la Plataforma, incluidos los Servicios del Profesional, es solo para su uso personal y que no está utilizando la Plataforma o los Servicios del Profesional en nombre de ninguna otra persona u organización.
                            Usted acepta y se compromete a no interferir o interrumpir, o intentar interferir o interrumpir, cualquiera de nuestros sistemas, servicios, servidores, redes o infraestructura, o cualquiera de los sistemas, servicios, servidores, redes o infraestructura de la Plataforma, incluidos, entre otros. obtener acceso no autorizado a lo mencionado anteriormente.
                            Usted acepta y se compromete a no hacer ningún uso de la Plataforma para publicar, enviar o entregar cualquiera de los siguientes: (a) correo electrónico no solicitado y / o publicidad o promoción de bienes y servicios; (b) software o código malicioso; (c) contenido ilegal, acosador, que invade la privacidad, abusivo, amenazante, vulgar, obsceno, racista o potencialmente dañino; (d) cualquier contenido que infrinja un derecho de terceros, incluidos los derechos de propiedad intelectual; (e) cualquier contenido que pueda causar daño a un tercero; (f) cualquier contenido que pueda constituir, causar o alentar una acción criminal o violar cualquier ley aplicable.
                            Usted acepta y se compromete a no violar ninguna ley, estatuto, ordenanza, norma, regulación o código ético local o internacional aplicable en relación con su uso de la Plataforma y su relación con los Profesionals y nosotros.
                            Si recibe algún archivo nuestro o de un Profesional, ya sea a través de la Plataforma o no, acepta verificar y analizar este archivo en busca de virus o software malicioso antes de abrir o usar este archivo.
                            Usted se compromete a no grabar o registrar de ninguna forma cualquier interacción con su Psicólogo (a) mediante la Plataforma sin su consentimiento.
                        </p>
                        <p>
                            Nos indemnizará, defenderá y eximirá de toda responsabilidad contra cualquier reclamo, pérdida, causas de acción, demandas, responsabilidades, costos o gastos (incluidos, entre otros, litigios y honorarios y gastos razonables de abogados) que surja de o esté relacionado con cualquiera de los siguientes: (a) su acceso o uso de la Plataforma; (b) cualquier acción realizada con su cuenta o Acceso a la cuenta ya sea por usted o por otra persona; (c) su violación de cualquiera de las disposiciones de este Acuerdo; (d) falta de pago por cualquiera de los servicios (incluidos los Servicios de Profesional) que se proporcionaron a través de la Plataforma; (e) su violación de cualquier derecho de terceros, incluidos, entre otros, cualquier derecho de propiedad intelectual, publicidad, confidencialidad, propiedad o derecho de privacidad. Esta cláusula sobrevivirá al vencimiento o terminación de este Acuerdo.
                        </p>
                        <h4> QUINTA.- Modificaciones, terminación, interrupción de la plataforma </h4>
                        <p>
                            Usted comprende, acepta y reconoce que podemos modificar, suspender, interrumpir la Plataforma, cualquier parte de la Plataforma o el uso de la Plataforma, ya sea para todos los clientes o para usted específicamente, en cualquier momento con o sin previo aviso. Usted acepta y reconoce que no seremos responsables de ninguna de las acciones mencionadas anteriormente ni de ninguna pérdida o daño causado por cualquiera de las acciones mencionadas anteriormente.
                            La Plataforma depende de varios factores, tales como software, hardware y herramientas, ya sean nuestros o de nuestros contratistas y proveedores. Si bien hacemos esfuerzos comercialmente razonables para garantizar la confiabilidad y accesibilidad de la Plataforma, usted comprende y acepta que ninguna plataforma puede ser 100% confiable y accesible, por lo que no podemos garantizar que el acceso a la Plataforma sea ininterrumpido o que sea accesible y consistente o sin errores en todo momento.
                        </p>
                        <h4> SEXTA.- Los pagos </h4>
                        <p>
                            Usted confirma y acepta usar solo tarjetas de crédito u otros medios de pago (colectivamente "Medios de pago") que está debidamente y totalmente autorizado a usar, y que toda la información relacionada con el pago que proporcionó y proporcionará en el futuro, a través de La plataforma es precisa, actual y correcta y continuará siendo precisa, actual y correcta.
                            Usted acepta pagar todas las tarifas y cargos asociados con su Cuenta de manera oportuna y de acuerdo con el programa de tarifas, los términos y las tarifas que se publican en la Plataforma. Al proporcionarnos sus Medios de pago, nos autoriza a facturar y cobrarle a través de dichos Medios de pago y acepta mantener información válida de Medios de pago en la información de su Cuenta.
                        </p>
                        <h4> SEPTIMA.- Avisos </h4>
                        <p>
                            Podemos proporcionarle avisos u otras comunicaciones con respecto a este acuerdo o cualquier aspecto de la Plataforma, por correo electrónico a la dirección de correo electrónico que tenemos registrada, por correo ordinario o publíquelo en línea. La fecha de recepción se considerará la fecha en que se da dicha notificación. Los avisos que nos envíen deben enviarse por correo electrónico a contacto@grupoconscientia.cl.
                        </p>
                        <h4> Notas importantes sobre nuestro Acuerdo </h4>
                        <p>Podemos cambiar este Acuerdo publicando modificaciones en la Plataforma. A menos que especifiquemos lo contrario, todas las modificaciones serán efectivas al momento de su publicación. Por lo tanto, le recomendamos que consulte los términos de este Acuerdo con frecuencia. La última fecha de actualización de este Acuerdo se publica en la parte superior del Acuerdo. Al usar la Plataforma después de que los cambios entren en vigencia, usted acepta estar sujeto a dichos cambios al Acuerdo. Si no está de acuerdo con los cambios, debe finalizar el acceso a la Plataforma y la participación en sus servicios.
                        </p>
                        <p>
                            Podemos transferir o asignar libremente este Acuerdo o cualquiera de sus obligaciones a continuación.
                            Los encabezados de los párrafos de este Acuerdo son únicamente por conveniencia y no se aplicarán en la interpretación de este Acuerdo.
                            Si un tribunal de jurisdicción competente considera que alguna disposición de este Acuerdo es ilegal, inválida, inaplicable o contraria a la ley, las disposiciones restantes de este Acuerdo permanecerán en pleno vigor y efecto.
                            Para aclarar cualquier duda, todas las cláusulas sobre limitaciones de responsabilidad e indemnización seguirán vigentes después de la terminación o vencimiento de este Acuerdo.
                        </p>

                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--Fin contenido home-->
    <?php include 'footer-line.php' ?>
    <?php include 'footer.php' ?>