<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);


$stmta = $db->prepare(buildQuerySelectPerfil());
$stmta->execute(array($_SESSION["id_usuario"]));
$perfil = $stmta->fetch(PDO::FETCH_ASSOC);

?>
<?php include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-dark bg-gradient-primary-to-secondary mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="edit-2"></i></div>
                                        Editar mi perfil
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                            <!--Informacion personal-->
                            <div id="infpersonal">
                                <div class="card mb-4">
                                    <div class="card-header">Información del Perfil</div>
                                    <?php if(isset($_GET["m"]) && $_GET["m"] == 1){  ?>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        Éxito actualizando sus datos.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                    <?php } ?>
                                    <?php if(isset($_GET["e"]) && $_GET["e"] == 1){  ?>
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        Debe completar todos los campos.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <?php } ?>

                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <form action="masters/perfiles/actualizar_cliente.php" method="POST">
                                                    <div class="form-group row">
                                                        <?php /*
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 text-center">
                                                            <label><strong>Foto de perfil</strong></label>
                                                            <br>
                                                            <img src="https://picsum.photos/200/200?random=<?php echo (rand(1, 10)); ?>" class="rounded-circle mb-3" alt="profile pic">
                                                            <input type="file" class="d-block mx-auto">
                                                        </div>
                                                        */?>
                                                        
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Nombres</strong></label>
                                                            <input class="form-control form-perfil" value="<?php echo utf8_encode($perfil["tx_nombre"]); ?>" type="text" name="nombre" id="nombre"  required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Apellidos</strong></label>
                                                            <input class="form-control form-perfil" value="<?php echo utf8_encode($perfil["tx_apellido"]); ?>" type="text" name="apellido" id="apellido"  required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>RUT</strong></label>
                                                            <input id="rut" class="form-control form-perfil" value="<?php echo utf8_encode($perfil["tx_rut"]); ?>" name="rut" id="rut"  type="text" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Correo Electrónico</strong></label>
                                                            <input class="form-control form-perfil" value="<?php echo utf8_encode($perfil["tx_correo"]); ?>" type="email" name="correo" id="correo"  required />
                                                        </div>

                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Teléfono</strong></label>
                                                            <input class="form-control form-perfil" value="<?php echo utf8_encode($perfil["tx_telefono"]); ?>" type="tel" name="telf" id="telf"  required />
                                                        </div>

                                                    </div>
                                                    <button type="button" class="btn btn-warning mb-2 unlock-fields">Desbloquear</button>
                                                    <button type="button" class="btn btn-link text-danger mb-2 save-buttons cancel">Cancelar</button>
                                                    <button type="submit" class="btn btn-primary mb-2 save-buttons guardar-perfil">Guardar</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="infcontrasena">
                                <div class="card mb-4">
                                    <div class="card-header">Contraseña</div>
                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <form action="masters/perfiles/actualizar_password.php" method="POST">

                                                    <div class="form-group row mb-3">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Contraseña nueva</strong></label>
                                                            <input class="form-control" type="password" name="password" />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Repite tu contraseña nueva</strong></label>
                                                            <input class="form-control" type="password" name="password_conf" />
                                                        </div>

                                                    </div>

                                                    <button type="submit" class="btn btn-primary mb-2">Guardar</button>
                                                    <?php if(isset($_GET["e"]) && $_GET["e"] == 2){  ?>
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        Las contraseñas no coinciden
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if(isset($_GET["m"]) && $_GET["m"] == 2){  ?>
                                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                        Éxito, contraseña actualizada correctamente.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>

    <script>
$(document).ready(function () {
    $(".form-perfil").attr("readonly",true);
    $(".save-buttons").fadeOut();
    $(".unlock-fields").on('click',function(){ // desbloquear campos
        $(".form-perfil").attr("readonly",false);
        $(".save-buttons").fadeIn();
        $(".unlock-fields").fadeOut();
    });
    $(".cancel").on('click',function(){  // cancelar edición campos
        $(".form-perfil").attr("readonly",true);
        $(".save-buttons").fadeOut();
        $(".unlock-fields").fadeIn();
    });
});
    </script>