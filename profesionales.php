<?php

include 'commons/funciones.php';
include 'commons/schema.php';

$db = connectToDB($dbData);

include 'header.php' ?>
<?php include 'topnav-front.php'


?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="row">
        <div class="col-12 mb-4 text-center">
            <img class="w-100" src="assets/img/banner-profesionales-min.jpg" alt="Grupo Conscientia - Centro de atención psicológica - Agenda una sesión con nuestros profesionales. Tu salud mental es nuestra prioridad.">
        </div>
    </div>
    <!-- seccion profesionales -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 mb-5">
                <div class="card-deck">

                    <div class="card text-center shadow-none bg-transparent border-0">
                        <div class="card-body text-center ">
                            <i class="fas fa-users fa-5x mb-3 color-bermuda"></i>
                            <h2><strong>Profesionales seleccionados</strong></h2>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>

                    <div class="card text-center shadow-none bg-transparent border-0">
                        <div class="card-body text-center ">
                            <i class="fas fa-file-invoice-dollar fa-5x mb-3 color-bermuda"></i>
                            <h2><strong>Sin subscripciones</strong></h2>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>

                    <div class="card text-center shadow-none bg-transparent border-0">
                        <div class="card-body text-center ">
                            <i class="fas fa-laptop fa-5x mb-3 color-bermuda"></i>
                            <h2><strong>Citas por videollamada</strong></h2>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>

                    <div class="card text-center shadow-none bg-transparent border-0">
                        <div class="card-body text-center ">
                            <i class="fas fa-credit-card fa-5x mb-3 color-bermuda"></i>
                            <h2><strong>Todo medio de pago</strong></h2>
                            <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="display-4 mb-4 font-bold">Nuestros profesionales</h2>
            </div>
            <div class="col-12 mb-3">
                <div class="card shadow-none p-2">
                    <form>
                        <div class="form-row align-items-center justify-content-center">

                            <div class="col-auto">
                                <p class="mb-1"><small>Buscar por <strong>nombre</strong></small></p>
                                <div class="input-group">
                                    <div class="input-group-prepend mb-0">
                                        <div class="input-group-text">🔍</div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Buscar por nombre ⏎">
                                </div>
                            </div>

                            <div class="col-auto py-1">
                                <p class="mb-1"><small>Filrar por <strong>especialidad</strong></small></p>
                                <select id="choices-multiple-remove-button" placeholder="Seleccione las especialiades que desea filtrar" multiple>
                                    <option value="Psicólogo">Psicólogo</option>
                                    <option value="Psiquiatra">Psiquiatra</option>
                                    <option value="Fonoaudiólogo">Fonoaudiólogo</option>
                                </select>
                            </div>

                            <div class="col-auto py-1">
                                <p class="mb-1"><small>Filtrar por <strong>motivo de consulta</strong></small></p>
                                <select id="choices-multiple-remove-button" placeholder="Seleccione las especialiades que desea filtrar" multiple>
                                    <option value="depresion">Depresion</option>
                                    <option value="insomnio">Insomnio</option>
                                    <option value="ansiedad">Ansiedad</option>
                                </select>
                            </div>



                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">

            <?php include 'profile-items.php' ?>


        </div>

        <!--paginacion-->
        <div class="row">
            <div class="col-12 d-flex mb-5 justify-content-center">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="#">Anterior</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">Siguiente</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <hr>


        <!--quienes somos-->

        <div class="row justify-content-center my-5 bg-bermuda py-5">
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 mb-3 text-center">
                <h4 class="text-uppercase mb-4 font-bold text-white">Quienes Somos</h4>
                <blockquote class="blockquote">
                    <h1 class="mb-5 text-indigo display-4">Grupo Conscientia es una organización formada por profesionales de la salud mental que disfruta de la colaboración y complemento con diversas disciplinas a fin de mejorar la calidad de vida humana.</h1>
                </blockquote>
            </div>
            <div class="col-12">&nbsp;</div>
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 mb-3 d-flex flex-row justify-content-center ">
                <div class="card bg-transparent shadow-none border-0 text-center m-3">
                    <img src="assets/img/elvira-espinoza.png">
                    <div class="card-body">
                        <h6>Elvira Espinoza</h6>
                        <p>Psicóloga, fundadora de Grupo Conscientia</p>
                    </div>
                </div>
                <div class="card bg-transparent shadow-none border-0 text-center m-3">
                    <img src="assets/img/constanza-contreras.png">
                    <div class="card-body">
                        <h6>Constanza Contreras</h6>
                        <p>Psicóloga, fundadora de Grupo Conscientia</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h4 class="text-uppercase mb-4 font-bold text-muted">Preguntas Frecuentes</h4>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 mb-3">
                <div class="accordion" id="accordionExample">

                    <div class="card shadow-none">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    ¿Puedo usar los beneficios de mi ISAPRE, FONASA o seguro complementario?
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                Some placeholder content for the first accordion panel. This panel is shown by default, thanks to the <code>.show</code> class.
                            </div>
                        </div>
                    </div>

                    <div class="card shadow-none">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    ¿Cómo son seleccionados los psicólogos?
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                Some placeholder content for the second accordion panel. This panel is hidden by default.
                            </div>
                        </div>
                    </div>

                    <div class="card shadow-none">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                                <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    ¿Funciona la terapia online?
                                </button>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>






    </div>



    <!--sección -->
    <!--Fin contenido home-->

    <?php include 'footer-line.php' ?>

    <?php include 'footer.php' ?>