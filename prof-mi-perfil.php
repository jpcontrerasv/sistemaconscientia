<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectCategoriasPerfil());
$stmta->execute(array($_SESSION["id_usuario"]));
$categoriasPerfilObjeto = $stmta->fetchAll(PDO::FETCH_ASSOC);
$categoriasPerfil = (array_column($categoriasPerfilObjeto, 'id_categoria'));

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$categorias = $stmta->fetchAll(PDO::FETCH_ASSOC);

$stmta = $db->prepare(buildQuerySelectPerfil());
$stmta->execute(array($_SESSION["id_usuario"]));
$perfil = $stmta->fetch(PDO::FETCH_ASSOC);

$carpeta_usuario = md5($_SESSION["id_usuario"]);
$directorio_objetivo = "uploads/" . $carpeta_usuario . "/";

?>
<?php include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-dark bg-gradient-primary-to-secondary mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="edit-2"></i></div>
                                        Editar mi perfil
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-9">
                            <!--Informacion personal-->
                            <div id="infpersonal">
                                <div class="card mb-4">
                                    <div class="card-header">Información del Perfil</div>
                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <form id="formInformacionPersonal">
                                                    <div class="form-group row">
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5 text-center">
                                                            <label><strong>Foto de perfil </strong></label>
                                                            <br>
                                     

                                                            <div class="d-block mx-auto m-4 bg-bermuda rounded-circle bg-cover" style="width:300px; <?php echo (!strpos($perfil["imagen_perfil"], ".") ? "display:none;" : "") ?> height:300px; background-image:url(<?php echo $directorio_objetivo . $perfil["imagen_perfil"] ?>);">&nbsp;</div>

                                                            <input type="file" name="perfil" class="d-block mx-auto">
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Nombres</strong></label>
                                                            <input class="form-control" name="nombre" value="<?php echo $perfil["tx_nombre"] ?>" type="text" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Apellidos</strong></label>
                                                            <input class="form-control" name="apellido" value="<?php echo $perfil["tx_apellido"] ?>" type="text" required />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>RUT</strong></label>
                                                            <input id="rut" name="rut" class="form-control" value="<?php echo $perfil["tx_rut"] ?>" type="text" required />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Correo Electrónico</strong></label>
                                                            <input class="form-control" name="correo" value="<?php echo $perfil["tx_correo"] ?>" type="email" required />
                                                        </div>
                                                        
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Teléfono</strong></label>
                                                            <input class="form-control form-perfil" value="<?php echo ($perfil["tx_telefono"]); ?>" type="tel" name="telf" id="telf"  required />
                                                        </div>

                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Registro del MINSAL</strong></label>
                                                            <input class="form-control" name="minsal" id="minsal" value="<?php echo ($perfil["tx_minsal"]); ?>" type="text" required />
                                                        </div>

                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <div class="form-group">
                                                                <label><strong>Descripción de mi perfil profesional </strong></label>
                                                                <textarea class="form-control" name="descripcion" name="textarea" id="textareaProf" maxlength="1000"><?php echo $perfil["tx_descripcion"] ?></textarea>
                                                                <span id="chars">1000</span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                    <button type="submit" class="btn btn-primary mb-2">Guardar</button>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Para actualizacion de contraseña -->
                            <div id="infcontrasena">
                                <div class="card mb-4">
                                    <div class="card-header">Contraseña</div>
                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <form action="masters/perfiles/actualizar_password.php" method="POST">

                                                    <div class="form-group row mb-3">
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Contraseña nueva</strong></label>
                                                            <input class="form-control" type="password" name="password" />
                                                        </div>
                                                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Repite tu contraseña nueva</strong></label>
                                                            <input class="form-control" type="password" name="password_conf" />
                                                        </div>

                                                    </div>

                                                    <button type="submit" class="btn btn-primary mb-2">Guardar</button>
                                                    <?php if(isset($_GET["e"]) && $_GET["e"] == 2){  ?>
                                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                        Las contraseñas no coinciden
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <?php } ?>
                                                    <?php if(isset($_GET["m"]) && $_GET["m"] == 2){  ?>
                                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                        Éxito, contraseña actualizada correctamente.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>