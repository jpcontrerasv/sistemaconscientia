<?php
session_start();
if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok") {
    header("Location: index.php");
    die();
}
include 'header.php' ?>

<body>

    <div id="layoutAuthentication">
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <!-- Basic login form-->
                            <div class="card shadow-none border-0 rounded-lg mt-5">
                                <div class="card-header text-center">
                                    <a href="index.php">
                                        <img src="assets/img/logo.png" class="w-50 mx-auto">
                                    </a>
                                </div>
                                <div class="card-header pb-1 text-center">
                                    <h3 class="font-weight-bold">Login</h3>
                                </div>
                                <div class="card-body">
                                    <!-- Login form-->
                                    <form action="val_login.php" method="POST">
                                        <!-- Form Group (email address)-->
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputEmailAddress">Email</label>
                                            <input class="form-control" type="text" placeholder="Ingresa tu email" name="correo" />
                                        </div>

                                        <!-- Form Group (password)-->
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputPassword">Contraseña</label>
                                            <div class="input-group" id="show_hide_password">
                                                
                                                <input class="form-control" id="remember_me" type="password" name="pass" placeholder="Ingresa tu contraseña" autocomplete="on" />

                                                <div class="input-group-addon d-flex align-items-center p-2 bg-light rounded-right border">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Form Group (remember password checkbox)-->
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                                                <label class="custom-control-label" for="rememberPasswordCheck">Recordar Contraseña</label>
                                            </div>
                                        </div>
                                        <!-- Form Group (login box)-->
                                        <div class="form-group d-flex flex-column align-items-center justify-content-between mt-4 mb-0">

                                            <button class="btn btn-primary lift mb-2" type="submit">Iniciar sesión</button>
                                            <a class="small" href="recordar-contrasena.php">¿Olvidaste tu contraseña?</a>
                                        </div>
                                    </form>

                                    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                                        Acceso incorrecto
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>



                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

        <?php include 'footer-line.php' ?>

    </div>

    <?php include 'footer.php' ?>