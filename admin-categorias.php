<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);


include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content">
                            <div class="row align-items-center justify-content-between pt-3">
                                <div class="col-auto mb-3">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="edit-2"></i></div>
                                        Editar Categorías
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--Informacion categorias-->
                            <div id="infCats">
                                <!--Información de la tienda-->
                                <div class="card mb-4 shadow-none">
                                    <div class="card-body">
                                        <div class="sbp-preview">
                                            <div class="sbp-preview-content">
                                                <form id="form">
                                                    <div class="form-group row">
                                                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                            <label><strong>Tipos de profesional</strong></label><br>
                                                            <small class="d-block w-100 mb-2">Escriba los tipos de profesionales acá. </small>

                                                            <div class="d-flex justify-content-between py-3">
                                                                <input class="rounded border" id="textboxPro" placeholder="Ej: Coach, Psicologo">
                                                                <a class="btn btn-primary btn-sm add-cat" id="addPro">Agregar a la lista</a>
                                                            </div>

                                                            <ul id="listPro" class="list-group">
                                                                <?php
                                                                foreach ($result as $categoria) {
                                                                    if ($categoria["tipo_categoria"] == 1) {
                                                                ?>
                                                                        <li categoria="<?php echo $categoria["id_categoria"] ?>" class="list-group-item d-flex justify-content-between align-items-center">
                                                                        <?php echo $categoria["nb_categoria"]; ?>
                                                                        <span class="badge badge-danger badge-pill del-categoria" categoria="<?php echo $categoria["id_categoria"] ?>">&nbsp;</span></li>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>

                                                        </div>
                                                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Previsiones</strong></label>
                                                            <small class="d-block w-100 mb-2">Escriba los tipos de previsiones acá.</small>

                                                            <div class="d-flex justify-content-between py-3">
                                                                <input class="rounded border" id="textboxPrev" placeholder="Ej: Fonasa, Particular">
                                                                <a class="btn btn-primary btn-sm add-cat" id="addPrev">Agregar a la lista</a>
                                                            </div>

                                                            <ul id="listPrev" class="list-group">
                                                                <?php
                                                                foreach ($result as $categoria) {
                                                                    if ($categoria["tipo_categoria"] == 2) {
                                                                ?>
                                                                        <li categoria="<?php echo $categoria["id_categoria"] ?>" class="list-group-item d-flex justify-content-between align-items-center"><?php echo ($categoria["nb_categoria"]); ?><span class="badge badge-danger badge-pill del-categoria" categoria="<?php echo $categoria["id_categoria"] ?>">&nbsp;</span></li>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>

                                                        </div>
                                                        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                            <label><strong>Motivos de consulta</strong></label>
                                                            <small class="d-block w-100 mb-2">Escriba los motivos de consulta acá. </small>

                                                            <div class="d-flex justify-content-between py-3">
                                                                <input class="rounded border" id="textboxCons" placeholder="Ej: Depresión, Fobia Social">
                                                                <a class="btn btn-primary btn-sm add-cat" id="addCons">Agregar a la lista</a>
                                                            </div>

                                                            <ul id="listCons" class="list-group">
                                                                <?php
                                                                foreach ($result as $categoria) {
                                                                    if ($categoria["tipo_categoria"] == 3) {
                                                                ?>
                                                                        <li categoria="<?php echo $categoria["id_categoria"] ?>" class="list-group-item d-flex justify-content-between align-items-center">
                                                                        
                                                                        <?php echo ($categoria["nb_categoria"]); ?><span class="badge badge-danger badge-pill del-categoria" categoria="<?php echo $categoria["id_categoria"] ?>">&nbsp;</span></li>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>