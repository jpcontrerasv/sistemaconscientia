<nav id="topnav-home" class="navbar navbar-expand-lg navbar-light bg-white">

    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="assets/img/logo.png" height="80">
        </a>

        <button class="navbar-toggler text-dark bg-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i data-feather="menu"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item m-1">
                    <a class="nav-link" href="https://grupoconscientia.cl/contacto/">Contacto</a>
                </li>

                <li class="nav-item m-1">
                    <a class="nav-link" href="profesionales.php">Centro de Atención Psicológica</a>
                </li>

                <?php if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok") { ?>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="logout.php"><small><small>Salir <i data-feather="log-out"></i></small></small></a>
                    </li>

                </a>

                <?php } else { ?>
                    <li class="nav-item m-1">
                        <a class="nav-link" href="login.php"><small><small>Acceder <i data-feather="log-in"></i></small></small></a>
                    </li>
                <?php } ?>



            </ul>

        </div>
    </div>
</nav>