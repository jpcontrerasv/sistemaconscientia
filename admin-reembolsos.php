<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";
$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);
?>
<?php include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content">
                            <div class="row align-items-center justify-content-between pt-3">
                                <div class="col-auto mb-3">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="rewind"></i></div>
                                        Reembolsos
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Tabla reembolsos solicitados-->
                            <div class="card shadow-none mb-4">
                                <div class="card-header">
                                    <h3>Histórico de reembolsos</h3>
                                </div>
                                <div class="card-body">
                                    <div class="datatable">
                                        <table class="table table-bordered table-hover" id="tableRH" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Nombre Profesional</th>
                                                    <th>Apellido Profesional</th>
                                                    <th>RUT Profesional</th>
                                                    <th>RUT Cliente</th>
                                                    <th>Mes Consulta</th>
                                                    <th>Día Consulta</th>
                                                    <th>Hora Consulta</th>
                                                    <th>Previsión Cliente</th>
                                                    <th>Total Pagado</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Nombre Profesional</th>
                                                    <th>Apellido Profesional</th>
                                                    <th>RUT Profesional</th>
                                                    <th>RUT Cliente</th>
                                                    <th>Mes Consulta</th>
                                                    <th>Día Consulta</th>
                                                    <th>Hora Consulta</th>
                                                    <th>Previsión Cliente</th>
                                                    <th>Total Pagado</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <tr>
                                                    <td>María Constanza</td>
                                                    <td>Contreras Valdés</td>
                                                    <td>15.999.666-1</td>
                                                    <td>15.962.195-2</td>
                                                    <td>Mayo</td>
                                                    <td>04</td>
                                                    <td>18:00 19:00</td>
                                                    <td>Particular</td>
                                                    <td>$15000</td>
                                                </tr>
                                                <tr>
                                                    <td>Elvira</td>
                                                    <td>Espinoza</td>
                                                    <td>15.121.333-4</td>
                                                    <td>18.111.444-k</td>
                                                    <td>Junio</td>
                                                    <td>12</td>
                                                    <td>14:00 15:00</td>
                                                    <td>Fonasa</td>
                                                    <td>$25000</td>
                                                </tr>
                                                <tr>
                                                    <td>María Constanza</td>
                                                    <td>Contreras Valdés</td>
                                                    <td>15.999.666-1</td>
                                                    <td>15.962.195-2</td>
                                                    <td>Mayo</td>
                                                    <td>04</td>
                                                    <td>18:00 19:00</td>
                                                    <td>Particular</td>
                                                    <td>$15000</td>
                                                </tr>
                                                <tr>
                                                    <td>Elvira</td>
                                                    <td>Espinoza</td>
                                                    <td>15.121.333-4</td>
                                                    <td>18.111.444-k</td>
                                                    <td>Junio</td>
                                                    <td>12</td>
                                                    <td>14:00 15:00</td>
                                                    <td>Fonasa</td>
                                                    <td>$25000</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </main>

            <?php include 'admin-footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>