<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Grupo Conscientia" />
    <meta name="author" content="Grupo Conscientia" />
    <meta name="robots" content="noindex" />
    <title>Grupo Conscientia</title>

    <link rel="icon" type="image/x-icon" href="assets/img/favicongc.png" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">

    <!--datepicker-->
    <link href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" rel="stylesheet" crossorigin="anonymous" />

    <!--bootstrap-->
    <link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />

    <!--sweet alert-->
    <link href="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.6/dist/sweetalert2.css" rel="stylesheet" crossorigin="anonymous" />

    <!--fancybox-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    <!--font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">

    <!--fileinput-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

    <!--feather-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>


    <!--estilos del admin-->
    <link href="css/styles.css?<?php echo (rand(10, 100)); ?>" rel="stylesheet" />


    <!--estilos en general-->
    <link href="css/custom-style.css?<?php echo (rand(1, 100)); ?>" rel="stylesheet" />

    <?php $items = array(64, 59, 55, 47, 45, 44, 43, 42, 38, 35, 12); ?>
    <!-- estilos del timepicker -->
    <link href="css/default.css" rel="stylesheet" />
    <link href="css/default.time.css" rel="stylesheet" />

    <!-- Estilos para el calendario y tabla de configuración de horas -->
    <link href="css/styles-calendar-conf.css" rel="stylesheet" />

    <?php date_default_timezone_set("America/New_York"); ?>

</head>