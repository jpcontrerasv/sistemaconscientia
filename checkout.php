<?php
session_start();
include 'commons/funciones.php';
include 'commons/schema.php';

// Inicio Codigo para renderizar pasarela
require_once "lib/khipu/Khipu.php";
$receiver_id = $id_receiver;
$llave = $secret_key;
$Khipu = new Khipu();
$Khipu->authenticate($receiver_id, $llave);
$khipu_service = $Khipu->loadService('CreatePaymentPage');
// Fin Codigo para renderizar pasarela ----

$db = connectToDB($dbData);

$id_profesional = $_POST["id_profesional"];
$fecha_reserva = $_POST["date-reservation"];
$id_cliente = $_SESSION["id_usuario"];
$hora_reserva = $_POST["hora_reserva"];
$dia_reserva = $_POST["field-day"];
$categoria = $_POST["select-categoria"];
$prevision = $_POST["select-prevision"];
$motivo = $_POST["select-motivo"];

$dia = explode("-", $fecha_reserva)[2];
$mes = explode("-", $fecha_reserva)[1];
$anio = explode("-", $fecha_reserva)[0];

$meses = [
    "01" => "enero",
    "02" => "febrero",
    "03" => "marzo",
    "04" => "abril",
    "05" => "mayo",
    "06" => "junio",
    "07" => "julio",
    "08" => "agosto",
    "09" => "septiembre",
    "10" => "octubre",
    "11" => "noviembre",
    "12" => "diciembre",
];


$stmta = $db->prepare(selectInfoProfesionalId());
$stmta->execute(array(2, 1, $id_profesional));
$result_profesional = $stmta->fetch(PDO::FETCH_ASSOC);



$stmta_cat = $db->prepare(getCategoriaPrecio());
$stmta_cat->execute(array($id_profesional, $categoria));
$result_cat = $stmta_cat->fetch(PDO::FETCH_ASSOC);



$stmta_prev = $db->prepare(getCategoriaPrecio());
$stmta_prev->execute(array($id_profesional, $prevision));
$result_prev = $stmta_prev->fetch(PDO::FETCH_ASSOC);
//consultar precios basados en especializacion + motivo
$precio = 0;
$precio_pasarela = 0;
if ($result_prev["nb_categoria"] == "Fonasa") {
    $precio = number_format($result_cat["precio_servicio"] - 4000, 0, ".", ",");
    $precio_pasarela = number_format($result_cat["precio_servicio"] - 4000, 0, ".", "");
} else {
    $precio = number_format($result_cat["precio_servicio"], 2, ".", ",");
    $precio_pasarela =  number_format($result_cat["precio_servicio"], 0, ".", "");
}

//Obtener ultimo valor de reserva para tener un identificador único que permita hacer la actualización en el notify.-


$return_url = $direccion_base . "/exito-reserva.php?id_cliente=".$id_cliente."&id_profesional=".$id_profesional."&fecha_reserva=".$fecha_reserva."&hora_inicio=".$hora_reserva."&hora_fin=".$hora_reserva."&id_categoria=".$categoria."&id_prevision=".$prevision."&id_motivo=".$motivo."&total_consulta=".$precio_pasarela."&id_dia=".$dia_reserva."&id_status=2&cod_transaccion=5";
$cancel_url = $direccion_base . "/error-reserva.php";
//$notify_url = $direccion_base . "/notificar.php";
$notify_url = "http://localhost/z/index.php";
$picture_url = $direccion_base . "/uploads/" . md5($id_profesional) . "/" . $result_profesional["imagen_perfil"];

// Luego de obtenido todos los valores, enviamos las variables para renderizar el botón de pago
$data = array(
    'subject' => 'Servicio de ' . utf8_encode($result_cat["nb_categoria"]),
    'body' => 'Cita reservada con: ' . utf8_encode($result_profesional["tx_nombre"] . " " . $result_profesional["tx_apellido"]) . ' el ' . $dia . " de " . $meses[$mes] . " de " . $anio . " - " . $hora_reserva,
    'amount' => $precio_pasarela,
    // Página de exito
    'return_url' => $return_url,
    // Página de fracaso
    'cancel_url' => $cancel_url,
    'transaction_id' => 1,
    // Dejar por defecto un correo para recibir el comprobante
    'payer_email' => $_SESSION["correo"],
    // url de la imagen del producto o servicio
    'picture_url' => $picture_url,
    // Opcional
    'custom' => 'Custom Variable',
    // definimos una url en donde se notificará del pago
    'notify_url' => $notify_url,
);
// Recorremos los datos y se lo asignamos al servicio.
foreach ($data as $name => $value) {
    $khipu_service->setParameter($name, $value);
}

include 'header.php' ?>
<?php include 'topnav-front.php' ?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="container">
        <div class="row">
            <div class="col-12 pb-2 text-left">
                <!-- Breadcrumb -->
                <nav aria-label="breadcrumb" class="main-breadcrumb mt-3">
                    <ol class="breadcrumb bg-white">
                        <li class="breadcrumb-item"><a href="javascript:history.back()"><i data-feather="chevrons-left"></i> Volver</a></li>
                    </ol>
                </nav>
                <!-- /Breadcrumb -->
                <h1 class="display-4">Checkout</h1>
                <?php /* <?php echo $precio_pasarela; ?> */ ?>
            </div>
            <div class="col-12">
                <!--resumen del carrito-->
                <div class="datatable mb-4">
                    <table class="table table-bordered table-hover rounded" id="tableCarrito" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Profesional</th>
                                <th>Servicio</th>
                                <th>Previsión</th>
                                <th>Hora Reservada</th>
                                <th>Total</th>
                                <!-- <th>Quitar</th> -->
                            </tr>
                        </thead>
                        <tbody class="bg-white">
                            <tr>
                                <td><?php echo ($result_profesional["tx_nombre"] . " " . $result_profesional["tx_apellido"]); ?></td>
                                <td><?php echo utf8_encode($result_cat["nb_categoria"]); ?></td>
                                <td><?php echo utf8_encode($result_prev["nb_categoria"]); ?></td>
                                <td><?php echo $dia . " de " . $meses[$mes] . " de " . $anio . " - " . $hora_reserva;  ?></td>
                                <td><span class="font-bold"><?php echo $precio; ?></span></td>
                                <!--<td class="text-center" width="10"><button class="borrar btn btn-danger btn-sm p-1"><i data-feather="x"></i></button></td> -->
                            </tr>
                        </tbody>
                    </table>

                </div>




                <div class="card shadow-none">
                    <div class="card-header">
                        <h3 class="mb-0">Pagar</h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 d-flex flex-column justify-content-start align-items-start mb-2">
                                    <!--<p class="m-0"><small><small>Todo medio de pago </small></small></p>
                                    <img class="w-100" src="assets/img/logos-pagos.png" alt="Todo medio de pago">-->
                                </div>

                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 d-flex justify-content-between align-items-center mb-2">
                                    <div>
                                        <p><small><small>Paga con Khipu usando todo medio de pago</small></small></p>
                                        <h4 class="p-0 m-0">
                                            Total: <strong>$<?php echo $precio; ?></strong>
                                        </h4>
                                    </div>
                                    <div>
                                        <p>&nbsp;</p>
                                        <?php echo $khipu_service->renderForm(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <p class="m-0"><small><small>Tu pago se realiza bajo un sistema seguro <i class="fas fa-lock fa-xs text-success"></i></small></small></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Fin contenido Checkout-->

    <?php include 'footer-line.php' ?>

    <?php include 'footer.php' ?>