<?php 
session_start(); 
 if(!isset($_SESSION["login"]) && $_SESSION["login"] != "ok"){  
        header("Location: login.php");
        die();
  }  
include "commons/funciones.php";
include "commons/schema.php";

$db = connectToDB($dbData);

$stmta = $db->prepare(buildQuerySelectPerfiles());
$stmta->execute(array());
$result = $stmta->fetchAll(PDO::FETCH_ASSOC);



include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content">
                            <div class="row align-items-center justify-content-between pt-3">
                                <div class="col-auto mb-3">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="user-check"></i></div>
                                        Todos los perfiles (85)
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Tabla-->
                            <div class="card shadow-none mb-4">
                                <div class="card-body">
                                    <div class="datatable">
                                        <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th>Nombres</th>
                                                    <th>Apellidos</th>
                                                    <th>RUT</th>
                                                    <th>Eliminar Cuenta</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Apellido</th>
                                                    <th>RUT</th>
                                                    <th>Eliminar Cuenta</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                    foreach ($result as $perfil) {
                                                ?>
                                                <tr usuario="<?php echo $perfil["id_usuario"]; ?>">
                                                    <td><?php echo utf8_encode($perfil["tx_nombre"]); ?></td>
                                                    <td><?php echo utf8_encode($perfil["tx_apellido"]); ?></td>
                                                    <td><?php echo $perfil["tx_rut"]; ?></td>
                                                    <td>
                                                        <button class="btn btn-danger btn-xs" type="button" data-fancybox data-src="#trueModal-<?php echo $perfil["id_usuario"]; ?>" data-modal="true" href="javascript:;">Eliminar Cuenta</button>
                                                        <div style="display: none;max-width:100vw;" id="trueModal-<?php echo $perfil["id_usuario"]; ?>" class=" card rounded-sm">
                                                            <h2>Elminar Cuenta</h2>
                                                            <form class="cerrarCuenta"  usuario="<?php echo $perfil["id_usuario"]; ?>">
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlSelect1">Elige el motivo</label>
                                                                    <select class="form-control razonEliminar" id="exampleFormControlSelect1">
                                                                        <option>Suplantación de identidad</option>
                                                                        <option>Decisión Propia del usuario</option>
                                                                        <option>Otros Motivos</option>
                                                                        <option>Fallecimiento</option>
                                                                    </select>
                                                                    <br>
                                                                    <p>Se enviará un correo notificando al usuario de su cierre de cuenta.</p>
                                                                    <p><strong>Esta acción es irreversible</strong>.</p>
                                                                </div>
                                                                <div class="form-group d-flex justify-content-between w-100 ">
                                                                    <button data-fancybox-close class="btn btn-link btn-sm">Cerrar ventana</button>
                                                                    <button usuario="<?php echo $perfil["id_usuario"]; ?>" data-fancybox-close class="btn btn-danger btn-lg cerrarCuentaButton">Cerrar cuenta</button>
                                                                    <small class="d-block text-right text-muted">[Dispara Mail [CUENTA ELIMINADA - 03A]]</small>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>