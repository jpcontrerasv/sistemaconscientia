<?php include 'header.php' ?>
<?php include 'topnav-front.php' ?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-5 text-center">
                <i class="fas fa-ban fa-3x text-danger mb-4"></i>
                <h1 class="display-4">Hubo un problema reservando tu hora </h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card p-4 mb-5 shadow-none">

                    <div class="card-body text-center">
                        <p>Algo salió mal intentando reservar una hora, por favor, inténtalo nuevamente</p>
                    </div>

                    <div class="card-footer text-center">
                        <a href="profesionales.php" class="btn btn-lg btn-primary btn-register">Volver a reservar</a>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--Fin contenido home-->
    <?php include 'footer-line.php' ?>
    <?php include 'footer.php' ?>