<?php
session_start();
if (!isset($_SESSION["login"]) && $_SESSION["login"] != "ok") {
    header("Location: login.php");
    die();
}
include "commons/funciones.php";
include "commons/schema.php";

$db = connectToDB($dbData);

$stmt_esp = $db->prepare(selectCategoriaProfesional());
$stmt_esp->execute(array($_SESSION["id_usuario"]));
$result_categoria = $stmt_esp->fetchAll();

$stmta = $db->prepare(buildQuerySelectCategoriasPerfil());
$stmta->execute(array($_SESSION["id_usuario"]));
$categoriasPerfilObjeto = $stmta->fetchAll(PDO::FETCH_ASSOC);
$categoriasPerfil = (array_column($categoriasPerfilObjeto, 'id_categoria'));

$stmta = $db->prepare(buildQuerySelectCategorias());
$stmta->execute(array());
$categorias = $stmta->fetchAll(PDO::FETCH_ASSOC);

$stmta = $db->prepare(buildQuerySelectPerfil());
$stmta->execute(array($_SESSION["id_usuario"]));
$perfil = $stmta->fetch(PDO::FETCH_ASSOC);

$carpeta_usuario = md5($_SESSION["id_usuario"]);
$directorio_objetivo = "uploads/" . $carpeta_usuario . "/";


include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">
        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>

                <header class="page-header page-header-dark bg-gradient-primary-to-secondary mb-4">
                    <div class="container-fluid">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="dollar-sign"></i></div>
                                        Mis servicios y tarifas
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <!-- Main page content-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">

                            <!--información tarifas-->
                            <div class="card mb-4">
                                <div class="card-header">Tarifas por hora de servicio</div>
                                <?php if (isset($_GET["m"]) && $_GET["m"] == 1) { ?>
                                    <div class="alert alert-primary alert-dismissible fade show mt-2 mb-2" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        Precio actualizado correctamente
                                    </div>

                                <?php } ?>



                                <div class="card-body mb-4">
                                    <div class="sbp-preview">
                                        <div class="container sbp-preview-content">
                                            <div class="row">
                                                <div class="tbl-serv">
                                                    <table class="table table-bordered table-hover" id="tableServ" width="100%" cellspacing="0">
                                                        <thead>
                                                            <tr>
                                                                <th>Servicio</th>
                                                                <th>Tarifa por hora (en $CLP)</th>
                                                                <th>Guardar cambios</th>
                                                            </tr>
                                                        </thead>
                                                        <tfoot>
                                                            <tr>
                                                                <th>Servicio</th>
                                                                <th>Tarifa por hora (en $CLP)</th>
                                                                <th>Guardar cambios</th>
                                                            </tr>
                                                        </tfoot>
                                                        <tbody>
                                                            <?php foreach ($result_categoria as $cat) {
                                                                # code...
                                                                if ($cat["tipo_categoria"] == 1) {


                                                            ?>
                                                                    <tr>
                                                                        <td><?php echo utf8_encode($cat["nb_categoria"]); ?></td>
                                                                        <td><input class="form-control" type="number" value="<?php echo $cat["precio_servicio"]; ?>" name="precio-serv-<?php echo $cat["id_categoria"] ?>" id="precio-serv-<?php echo $cat["id_categoria"] ?>"></td>
                                                                        <td>
                                                                            <button class="btn btn-warning btn-xs save-price" type="button" id="save-changes-price-<?php echo $cat["id_categoria"] ?>">Guardar cambios</button>

                                                                        </td>
                                                                    </tr>
                                                            <?php
                                                                }
                                                            } ?>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <?php   ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!--Informacion categorias-->
                            <div class="card mb-4">
                                <div class="card-header">Información Profesional</div>
                                <div class="card-body">
                                    <div class="sbp-preview">
                                        <div class="sbp-preview-content">
                                            <form id="formInformacionProfesional">
                                                <div class="form-group row">
                                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-2">
                                                        <label><strong>Especialidades</strong></label><br>
                                                        <small class="d-block w-100 mb-2">Selecciona tu o tus tipos de especialidad. </small>
                                                        <?php
                                                        foreach ($categorias as $categoria) {
                                                            $id_categoria = $categoria["id_categoria"];
                                                            if ($categoria["tipo_categoria"] == 1) {
                                                        ?>
                                                                <div class="form-check">
                                                                    <input <?php echo (in_array($id_categoria, $categoriasPerfil) ? "checked" : ""); ?> class="form-check-input" type="checkbox" value="Ok" name="defaultCheck<?php echo $id_categoria ?>" id="defaultCheck<?php echo $id_categoria ?>">
                                                                    <label class="form-check-label" for="defaultCheck<?php echo $id_categoria ?>">
                                                                        <?php echo utf8_encode($categoria["nb_categoria"]); ?>
                                                                    </label>
                                                                </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                        <label><strong>Previsiones con las que atiende</strong></label>
                                                        <small class="d-block w-100 mb-2">Seleccione los tipos de previsiones con los que atiende. </small>
                                                        <?php
                                                        foreach ($categorias as $categoria) {
                                                            $id_categoria = $categoria["id_categoria"];
                                                            if ($categoria["tipo_categoria"] == 2) {
                                                        ?>
                                                                <div class="form-check">
                                                                    <input <?php echo (in_array($id_categoria, $categoriasPerfil) ? "checked" : ""); ?> class="form-check-input" type="checkbox" value="Ok" name="defaultCheck<?php echo $id_categoria ?>" id="defaultCheck<?php echo $id_categoria ?>">
                                                                    <label class="form-check-label" for="defaultCheck<?php echo $id_categoria ?>">
                                                                        <?php echo utf8_encode($categoria["nb_categoria"]); ?>
                                                                    </label>
                                                                </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>

                                                    </div>
                                                    <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12 mb-3">
                                                        <label><strong>Motivos de consulta</strong></label>
                                                        <small class="d-block w-100 mb-2">Seleccione los tipos de consulta </small>
                                                        <?php
                                                        foreach ($categorias as $categoria) {
                                                            $id_categoria = $categoria["id_categoria"];
                                                            if ($categoria["tipo_categoria"] == 3) {
                                                        ?>
                                                                <div class="form-check">
                                                                    <input <?php echo (in_array($id_categoria, $categoriasPerfil) ? "checked" : ""); ?> class="form-check-input" type="checkbox" value="Ok" name="defaultCheck<?php echo $id_categoria ?>" id="defaultCheck<?php echo $id_categoria ?>">
                                                                    <label class="form-check-label" for="defaultCheck<?php echo $id_categoria ?>">
                                                                        <?php echo utf8_encode($categoria["nb_categoria"]); ?>
                                                                    </label>
                                                                </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary mb-2">Guardar</button>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </main>

            <?php include 'footer-line.php' ?>

        </div>
    </div>

    <?php include 'footer.php' ?>

    <script src="js/calendario-conf.js"></script>

    <script>
        $(document).ready(function() {
            $('.save-price').on('click', function() {
                var id = $(this).attr('id').split('-')[3];
                var precio = $("#precio-serv-" + id).val();
                $.ajax({
                    url: "masters/perfiles/save-price.php",
                    type: "POST",
                    data: {
                        id_categoria: id,
                        precio: precio
                    },
                    success: function(response) {
                        //let data = JSON.parse(response);
                        //console.log(data.nombre);
                        console.log(response);

                    },
                });
            });

        });
    </script>