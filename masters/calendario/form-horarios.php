<?php
    
    session_start();

    include '../../commons/funciones.php';
    include '../../commons/schema.php';
    $dias = [
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
        'Domingo'
    ];
    $dia = $_POST["id_dia"];
    
    $db = connectToDB($dbData);

    $stmta = $db->prepare(selectDiaCalendarioUser());
    $stmta->execute(array($_SESSION["id_usuario"],$dia));
    $result_horario = $stmta->fetchAll();
// determinar fechas del día para el usuario y montar en la tabla.
    
$formulario = "";
$filas = 0;
$j=1;

$horas_deshabilitadas = [];
//echo count($result_horario).'----<br>';

if(count($result_horario) == 0){
    $filas = 1;
    $formulario .= '


                <tr style="font-size:9px !important;">
                    <td></td>
                    <td><input class="hora hora_inicio timepicker form-control" name="0_hora_inicio-1" id="0_hora_inicio-1" value=""></td>
                    <td><input class="hora hora_final timepicker form-control" name="0_hora_fin-1" id="0_hora_fin-1" value=""></td>
                    <td>
                        <i id="" class="fa fa-times remove_button" style="padding:3px;color:white;background-color:#f44336;border-radius:2px;cursor:pointer;font-size:2em;"></i>

                    </td>
                </tr>
            ';
}else{
    
foreach ($result_horario as $hora) {
    $filas = 0;
    $hora_inicio = explode(":",$hora["hora_inicio"])[0].":".explode(":",$hora["hora_inicio"])[1];
    $hora_fin = explode(":",$hora["hora_fin"])[0].":".explode(":",$hora["hora_fin"])[1];

    $formulario .= '


                <tr style="font-size:9px !important;">
                    <td></td>
                    <td><input class="hora hora_inicio timepicker form-control" name="0_hora_inicio-'.$j.'" id="0_hora_inicio-'.$j.'" value="'.$hora_inicio.'"></td>
                    <td><input class="hora hora_final timepicker form-control" name="0_hora_fin-'.$j.'" id="0_hora_fin-'.$j.'" value="'.$hora_fin.'"></td>
                    <td>
                        <i id="" class="fa fa-times remove_button" style="padding:3px;color:white;background-color:#f44336;border-radius:2px;cursor:pointer;font-size:2em;"></i>

                    </td>
                </tr>
            ';
            $horas = array ("from"=>array((int)explode(":",$hora_inicio)[0],
            (int)explode(":",$hora_inicio)[1]
                                    )
    ,
                        "to"=>array((int)explode(":",$hora_fin)[0],
                        (int)explode(":",$hora_fin)[1]
));
    array_push($horas_deshabilitadas,$horas);

$j++;
}
}


if($filas == 0){
    $cant_filas = $j-1;
}
if($filas == 1){
    $cant_filas = 1;
}

$retorno= ["status" => "exito",
        	"formulario" =>$formulario,
            "dia" =>$dias[$dia-1],
            "horas_deshabilitadas" => $horas_deshabilitadas,
            "cant_filas" => $cant_filas
          ];

echo json_encode($retorno);

?>