<?php

include '../../commons/funciones.php';
include '../../commons/schema.php';

$dia = $_POST["id_dia"];
$id_profesional = $_POST["id_profesional"];    
$db = connectToDB($dbData);


$stmta = $db->prepare(selectDiaCalendarioUser());
$stmta->execute(array($id_profesional,$dia));
$result_horario = $stmta->fetchAll();
$horas_deshabilitadas = [array ("from"=>array(0,0),
"to"=>array(23,30))];
foreach ($result_horario as $hora) { 
    
    $hora_inicio = explode(":",$hora["hora_inicio"])[0].":".explode(":",$hora["hora_inicio"])[1];
    $hora_fin = explode(":",$hora["hora_fin"])[0].":".explode(":",$hora["hora_fin"])[1];

    $horas = array ("from"=>array((int)explode(":",$hora_inicio)[0],
    (int)explode(":",$hora_inicio)[1]
                            )
,
                "to"=>array((int)explode(":",$hora_fin)[0],
                (int)explode(":",$hora_fin)[1]
),
"inverted"=>true);
array_push($horas_deshabilitadas,$horas);



} //foreach horas

$retorno= ["status" => "exito",
"horas_deshabilitadas" => $horas_deshabilitadas];
//var_dump($horas_deshabilitadas);

echo json_encode($retorno);