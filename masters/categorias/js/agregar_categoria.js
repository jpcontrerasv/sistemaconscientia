$(document).ready(function () {
	// var dataString = $("input#textboxPro").serialize();

	$('.add-cat').on("click", function () {
		var tipo_cat = $(this).attr("id");
		var valor = "";
		if(tipo_cat === "addPro"){
			valor = $("#textboxPro").val();
			//alert("entra en addpro: "+valor);
		}
		if(tipo_cat === "addPrev"){
			valor = $("#textboxPrev").val();
		}
		if(tipo_cat === "addCons"){
			valor = $("#textboxCons").val();
		}
			//alert("Entra en el aja1 " + valor);
		if (valor === "") {
			console.log("rellena el campo");
		} else {
			//console.log(dataString);
			agregar_registro(tipo_cat,valor);
			$('#textboxPro').val("");
			$('#textboxPrev').val("");
			$('#textboxCons').val("");
		}
	});

	$(document).on("click",'.del-categoria', function () {
		var id_categoria = $(this).attr("categoria");
		eliminar_registro_categoria(id_categoria);
		
	});


	function agregar_registro(tipo_cat,valor) {

		$.ajax({
			url: "masters/categorias/agregar_categoria.php",
			type: "POST",
			data: { 
				tipo_cat:tipo_cat,
				nombre:valor
			 },
			success: function (response) {
				 let data = JSON.parse(response);
				 console.log(data.nombre);
				 if(data.tipo_categoria === 1){
				 	$('ul#listPro').append('<li categoria="'+data.id+'" class="list-group-item d-flex justify-content-between align-items-center">' + data.nombre + '<span class="badge badge-danger badge-pill del-categoria" categoria="'+data.id+'">&nbsp;</span></li>');
				 }
				 if(data.tipo_categoria === 2){
				 	$('ul#listPrev').append('<li categoria="'+data.id+'" class="list-group-item d-flex justify-content-between align-items-center">' + data.nombre + '<span class="badge badge-danger badge-pill del-categoria" categoria="'+data.id+'">&nbsp;</span></li>');
				 }
				 if(data.tipo_categoria === 3){
					$('ul#listCons').append('<li categoria="'+data.id+'" class="list-group-item d-flex justify-content-between align-items-center">' + data.nombre + '<span class="badge badge-danger badge-pill del-categoria" categoria="'+data.id+'">&nbsp;</span></li>');
				 }
				// let template = "";
				// tasks.forEach((task) => {
				// 	template += `
				//             <li class="list-group-item d-flex justify-content-between align-items-center">${task.nb_categoria}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
				//     `;
				// });
				// $("#listPro").html(template);
			},
		});
	}

	function eliminar_registro_categoria(id) {
		//alert(id);
if (confirm("¿Desea remover la categoría?")) {
            $.ajax({
        url: "masters/categorias/eliminar_categoria.php",
        type: "POST",
        data: {
            id
        } ,
        success: function (response) {
            if(response === "Si"){
                $(`[categoria=${id}]`).remove()
            }else{
                alert("Ocurrió un error")
            }
            // let template = "";
            // tasks.forEach((task) => {
            //  template += `
            //             <li class="list-group-item d-flex justify-content-between align-items-center">${task.nb_categoria}<span class="badge badge-danger badge-pill">&nbsp;</span></li>
            //     `;
            // });
            // $("#listPro").html(template);
        },
    });
        } else {

        }
    


}




});
