<?php
session_start();

include '../../commons/funciones.php';
include '../../commons/schema.php';

//var_dump($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    
    //var_dump($id);

    $db = connectToDB($dbData);
    $stmta = $db->prepare(buildQueryDeleteCategoriaPerfil());
    $stmta->execute(array($_SESSION["id_usuario"]));
    
    $stmta = $db->prepare(buildQuerySelectCategorias());
    $stmta->execute(array());
    $categorias = $stmta->fetchAll();
    $resultado = true;
    foreach ($categorias as $categoria) {
        if(isset($_POST["defaultCheck".$categoria["id_categoria"]])){
            $stmta = $db->prepare(buildQueryInsertCategoriaPerfil());
            $stmta->execute(array($categoria["id_categoria"], $_SESSION["id_usuario"]));
            $result = $stmta->rowCount();
			if($result > 0){
			}else{
                $resultado = false;
			}
        }
    }
    if(!$resultado){
        $stmta = $db->prepare(buildQueryDeleteCategoriaPerfil());
        $stmta->execute(array($_SESSION["id_usuario"]));
        echo "No";
    }
    
}