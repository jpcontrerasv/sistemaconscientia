<?php
session_start();

include '../../commons/funciones.php';
include '../../commons/schema.php';

//var_dump($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $seguir = true;

    $db = connectToDB($dbData);
    $stmta = $db->prepare(buildQueryActualizarPerfil());

    $stmta->execute(array(
        $_POST["correo"],
        utf8_decode($_POST["nombre"]),
        utf8_decode($_POST["apellido"]),
        $_POST["rut"],
        utf8_decode($_POST["descripcion"]),
        ($_POST["telf"]),
        ($_POST["minsal"]),
        $_SESSION["id_usuario"]
    ));
    
    $resultado = $stmta->rowCount();
    $_SESSION["correo"] = $_POST["correo"];
    
    $carpeta_usuario = md5($_SESSION["id_usuario"]); 
    
    // Variable bandera para validacion de imágenes y subida
    //echo $carpeta_usuario.'<br>';
    $uploadOk = 1;
    $rutaAraiz = '../../';
    $directorio_objetivo = "uploads/".$carpeta_usuario."/";

    if(file_exists($rutaAraiz.$directorio_objetivo)){ // verifica que el directorio existe
        //echo "directorio ya existe<br>";
    }else{ // verifica que el directorio existe (lo crea en caso contrario)
        if(!mkdir($rutaAraiz.$directorio_objetivo, 0777, true)){
            echo 'No';
            $seguir = false;
        }
    }

    if($seguir){

        if (move_uploaded_file($_FILES["perfil"]["tmp_name"], $rutaAraiz.$directorio_objetivo.'perfil.'.(isset(explode(".", $_FILES["perfil"]["name"])[1]) ? explode(".", $_FILES["perfil"]["name"])[1] : "") )) {
            $stmta = $db->prepare(buildQueryActualizarFotoPerfil());

            $stmta->execute(array(
                'perfil.'.(isset(explode(".", $_FILES["perfil"]["name"])[1]) ? explode(".", $_FILES["perfil"]["name"])[1] : ""),
                $_SESSION["id_usuario"]
            ));
            
            $resultado = $stmta->rowCount();
            echo "Si";
            
            
        }else{
            echo "Solo informacion";
        }
    }else{
        echo "No";
    }
    
}