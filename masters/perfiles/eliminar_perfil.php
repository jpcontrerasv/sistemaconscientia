<?php
session_start();

include '../../commons/funciones.php';
include '../../commons/schema.php';

//var_dump($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = ($_POST["id"]);
    $motivo = ($_POST["motivo"]);
    //var_dump($id);

    $db = connectToDB($dbData);

    $stmta = $db->prepare(buildQueryDeletePerfil());
    $stmta->execute(array($id));
    $resultado = $stmta->rowCount();
    if($resultado > 0){
        $stmta = $db->prepare(buildQueryInsertMotivo());
        $stmta->execute(array($motivo, $id));
        $resultado = $stmta->rowCount();
        if($resultado > 0){
            echo "Si";
        }else{
            echo "Solo eliminado";
        }
        
    }else{
        echo "No";
    }
}