<?php

session_start();

include '../../commons/funciones.php';
include '../../commons/schema.php';

//var_dump($_POST);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

$nombre = utf8_decode($_POST["nombre"]);
$apellido = utf8_decode($_POST["apellido"]);
$rut = $_POST["rut"];
$correo = $_POST["correo"];
$telefono = $_POST["telf"];

if($nombre != "" && $apellido != "" && $rut != "" && $correo != "" && $telefono != ""){
    
	$db = connectToDB($dbData);

	$stmta = $db->prepare(buildQueryActualizarPerfil());
	$stmta->execute(array($correo, $nombre, $apellido, $rut,"",$telefono, "", $_SESSION["id_usuario"]));
	$resultado = $stmta->rowCount();

    header('Location: ../../cli-mi-perfil.php?m=1');
}else{
    header('Location: ../../cli-mi-perfil.php?e=1');
}




}