<?php  ?>
<!--Sidebar-->
<div id="layoutSidenav_nav">
    <nav class="sidenav shadow-right sidenav-light">
        <div class="sidenav-menu">
            <div class="nav accordion" id="accordionSidenav">
<!-- -------------------------------------------------------------------------------- -->                
                <!-- Aquí comienza el sidebar del administador-->
<?php if($_SESSION["id_rol"] == 1){

 ?>
                <!--Sidebar-->
                <div class="sidenav-menu-heading">Menu [Admin]</div>

                <!-- Sidenav Link-->
                <a class="nav-link collapsed" href="javascript:void(0);" data-toggle="collapse" data-target="#collapsePerfiles" aria-expanded="false" aria-controls="collapsePerfiles">
                    <div class="nav-link-icon"><i data-feather="users"></i></div>
                    Perfiles
                    <div class="sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse show" id="collapsePerfiles" data-parent="#accordionSidenav">
                    <nav class="sidenav-menu-nested nav accordion" id="accordionSidenavPages">
                        <a class="nav-link" href="admin-crear-perfil.php">Crear Perfil</a>
                        <a class="nav-link" href="admin-todos-los-perfiles.php">Todos los perfiles</a>
                        <a class="nav-link" href="admin-categorias.php">Categorias</a>
                    </nav>
                </div>

                <!-- Sidenav Link-->
                <a class="nav-link" href="admin-reportes.php">
                    <div class="nav-link-icon"><i data-feather="briefcase"></i></div>
                    Reportes
                </a>

                <!-- 
                <a class="nav-link" href="admin-reembolsos.php">
                    <div class="nav-link-icon"><i data-feather="rewind"></i></div>
                    Reembolsos
                </a>
                -->


                <!-- Sidenav Link-->
                <a class="nav-link text-danger" href="logout.php">
                    <div class="nav-link-icon text-danger"><i data-feather="log-out"></i></div>
                    Salir
                </a>
<?php  } ?>
<!-- -------------------------------------------------------------------------------- -->
                <!-- Aquí comienza el sidebar del profesional-->
<?php if($_SESSION["id_rol"] == 2){

 ?>
                <!--Sidebar-->
                <div class="sidenav-menu-heading">Menu [Profesional]</div>

                <!-- Sidenav Link-->
                <a class="nav-link" href="prof-mi-perfil.php">
                    <div class="nav-link-icon"><i data-feather="user"></i></div>
                    Mi Perfil
                </a>
                <a class="nav-link" href="prof-calendario.php">
                    <div class="nav-link-icon"><i data-feather="calendar"></i></div>
                    Mi calendario
                </a>
                
                
                <a class="nav-link" href="prof-servicios.php">
                    <div class="nav-link-icon"><i data-feather="dollar-sign"></i></div>
                    Mis servicios y tarifas
                </a>
                

                <!-- Sidenav Link-->
                <a class="nav-link text-danger" href="logout.php">
                    <div class="nav-link-icon text-danger"><i data-feather="log-out"></i></div>
                    Salir
                </a>
<?php  } ?>
<!-- -------------------------------------------------------------------------------- -->
                <!-- Aquí comienza el sidebar del cliente-->
<?php if($_SESSION["id_rol"] == 3){

 ?>
                <!--Sidebar-->
                <div class="sidenav-menu-heading">Menu [Cliente]</div>

                <!-- Sidenav Link-->
                <a class="nav-link" href="cli-mi-perfil.php">
                    <div class="nav-link-icon"><i data-feather="user"></i></div>
                    Mi Perfil
                </a>

                <!-- Sidenav Link-->
                <a class="nav-link" href="cli-horas.php">
                    <div class="nav-link-icon"><i data-feather="clock"></i></div>
                    Mis horas
                </a>

                <!-- Sidenav Link-->
                <a class="nav-link text-danger" href="logout.php">
                    <div class="nav-link-icon text-danger"><i data-feather="log-out"></i></div>
                    Salir
                </a>
<?php  } ?>


            </div>
        </div>
    </nav>
</div>