<nav id="topNavDashboards" class="topnav navbar navbar-expand shadow justify-content-between justify-content-sm-start navbar-light bg-white" id="sidenavAccordion">
    <!-- Navbar Brand-->
    <!-- * * Tip * * You can use text or an image for your navbar brand.-->
    <!-- * * * * * * When using an image, we recommend the SVG format.-->
    <!-- * * * * * * Dimensions: Maximum height: 32px, maximum width: 240px-->
    <a class="navbar-brand pt-0 pb-0" href="index.php">
        <img src="assets/img/logo.png">
    </a>
    <!--
    <select class="custom-select" id="inlineFormCustomSelect">
        <option selected>Más nuevas</option>
        <option value="1">A - Z</option>
        <option value="1">Z - A</option>
        <option value="1">Con más galerías</option>
        <option value="1">Con menos galerías</option>
        <option value="1">Más Vendedoras</option>
        <option value="1">Material más nuevo</option>
    </select>
-->
    <!-- Sidenav Toggle Button-->
    <button class="btn btn-icon btn-transparent-dark order-1 order-lg-0 mr-lg-2" id="sidebarToggle"><i data-feather="menu"></i></button>
</nav>