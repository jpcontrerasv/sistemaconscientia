<?php
session_start();
if (isset($_SESSION["login"]) && $_SESSION["login"] == "ok") {
    header("Location: index.php");
    die();
}
include 'header.php' ?>

<body>

    <div id="layoutAuthentication">
        <div id="layoutSidenav_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <!-- Breadcrumb -->
                            <nav aria-label="breadcrumb" class="main-breadcrumb mt-3">
                                <ol class="breadcrumb bg-white">
                                    <li class="breadcrumb-item"><a class="text-primary" href="login.php"><i data-feather="chevrons-left"></i> Volver al login</a></li>
                                </ol>
                            </nav>
                            <!-- /Breadcrumb -->

                            <!-- Basic login form-->
                            <div class="card shadow-none border-0 rounded-lg mt-5">
                            <div class="card-header text-center">
                                    <a href="index.php">
                                        <img src="assets/img/logo.png" class="w-50 mx-auto">
                                    </a>
                                </div>
                                <div class="card-header pb-1 text-center">
                                    <h3 class="font-weight-bold">Cambiar contraseña</h3>
                                </div>


                                <div class="card-body">
                                    <!-- Login form-->
                                    <form action="val_login.php" method="POST">

                                       

                                        <!-- Form Group (password)-->
                                        <div class="form-group">
                                            <label class="small mb-1" for="newpass">Contraseña nueva</label>
                                            <div class="input-group" id="show_hide_password">
                                                
                                                <input class="form-control" id="newpass" type="password" name="pass" placeholder="Ingresa tu nueva contraseña" autocomplete="on" />

                                                <div class="input-group-addon d-flex align-items-center p-2 bg-light rounded-right border">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Form Group (password)-->
                                        <div class="form-group">
                                            <label class="small mb-1" for="repeatNewPass">Repite tu contraseña nueva</label>
                                            <div class="input-group" id="show_hide_password_b">
                                                
                                                <input class="form-control" id="repeatNewPass" type="password" name="pass" placeholder="Repite tu contraseña nueva" autocomplete="on" />

                                                <div class="input-group-addon d-flex align-items-center p-2 bg-light rounded-right border">
                                                    <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>

    

                                        <!-- Form Group (login box)-->
                                        <div class="form-group d-flex flex-column align-items-center justify-content-between mt-4 mb-0">

                                            <button class="btn btn-primary lift mb-2" type="submit">Guardar</button>
                                        </div>
                                    </form>

                                    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                                        Contraseñas no coinciden
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                                        Éxito. <a href="login.php">Volver al login</a>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>



                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

        <?php include 'footer-line.php' ?>

    </div>

    <?php include 'footer.php' ?>