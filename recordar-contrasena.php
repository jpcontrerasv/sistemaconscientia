<?php include 'header.php' ?>

<body>

    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container">

                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <!-- Breadcrumb -->
                            <nav aria-label="breadcrumb" class="main-breadcrumb mt-3">
                                <ol class="breadcrumb bg-white">
                                    <li class="breadcrumb-item"><a class="text-primary" href="login.php" ><i data-feather="chevrons-left"></i> Volver</a></li>
                                </ol>
                            </nav>
                            <!-- /Breadcrumb -->

                            <!-- Basic forgot password form-->
                            <div class="card shadow-none border-0 rounded-lg mt-5">
                                <div class="card-header text-center">
                                    <a href="index.php">
                                        <img src="assets/img/logo.png" class="w-50 mx-auto">
                                    </a>
                                </div>
                                <div class="card-header pb-1 text-center">
                                    <h3 class="font-weight-bold">Recupera tu password</h3>
                                </div>
                                <div class="card-body">
                                    <!-- Forgot password form-->
                                    <form>
                                        <!-- Form Group (email address)-->
                                        <div class="form-group">
                                            <label class="small mb-1">Ingresa el email que usaste para registrarte.</label>
                                            <input class="form-control" type="email" required />
                                        </div>
                                        <!-- Form Group (submit options)-->
                                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                            <input class="btn btn-primary" type="submit" value="Enviar enlace">
                                        </div>
                                    </form>

                                    <br>
                                    

                                    <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
                                        Si el mail ingresado existe te enviaremos las instrucciones al correo indicado
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <small class="text-muted">[En caso de éxito: mail <a href="mails-transaccionales.php">02A</a>]</small>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

        <?php include 'footer-line.php' ?>
    </div>

    <?php include 'footer.php' ?>