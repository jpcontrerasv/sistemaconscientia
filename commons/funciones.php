<?php

function connectToDB($db)
{
    //$mysqli = new mysqli($db['host'], $db['username'], $db['password'], $db['schema']);
    //return $mysqli->connect_errno ? NULL : $mysqli;
    $host = $db['host'];
    $bd = $db['schema'];
    try {
        // Conectar
        $db = new PDO("mysql:host=$host;dbname=$bd;", $db['username'], $db['password']);
        // Establecer el nivel de errores a EXCEPTION
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
    } catch (PDOException $e) {
        return "Error: " . $e->getMessage();
    }
}


//usuarios


function selectUsuarioLogin()
{
    return "SELECT * FROM gc_usuarios WHERE tx_correo = ? AND tx_password = ? AND id_status != ?";
}


//Categorias


function buildQuerySelectCategorias()
{
    return "SELECT * FROM gc_categorias";
}

function buildQueryInsertCategoria()
{
    return "INSERT INTO `gc_categorias` ( `tipo_categoria`, `nb_categoria`) VALUES (?, ?)";
}

function buildQueryDeleteCategoria()
{
    return "DELETE FROM `gc_categorias` WHERE id_categoria = ? ";
}

//Perfiles


function buildQuerySelectPerfiles()
{
    return "SELECT * FROM gc_usuarios WHERE id_rol != 1 AND id_status != 3" ;
}

function buildQueryInsertPerfil()
{
    return "INSERT INTO `gc_usuarios`(`tx_correo`, `tx_password`, `tx_nombre`, `tx_apellido`, `tx_rut`, `tx_descripcion`, `id_rol`, `id_status`, `tx_telefono`, `tx_minsal`) VALUES (  ?, ?, ?, ?, ?, '', ?, 1, ?, ?  )";
}
function buildQueryDeletePerfil()
{
    return "UPDATE `gc_usuarios` SET id_status = 3 WHERE id_usuario = ? ";
}

// Motivo eliminación
function buildQueryInsertMotivo()
{
    return "INSERT INTO `gc_motivo_eliminacion`(`tx_motivo`, `id_usuario`) VALUES (
        ?, ?
    )";
}

function buildQueryActualizarFotoPerfil()
{
    return "UPDATE `gc_usuarios` SET imagen_perfil = ? WHERE id_usuario = ? ";
}

// Categoría perfil


function buildQueryInsertCategoriaPerfil()
{
    return "INSERT INTO `gc_usuario_categoria` (`id_categoria`, `id_usuario`) VALUES (?, ?)";
}

function buildQueryDeleteCategoriaPerfil()
{
    return "DELETE FROM `gc_usuario_categoria` WHERE id_usuario = ? ";
}


function buildQuerySelectPerfil()
{
    return "SELECT * FROM `gc_usuarios` WHERE id_usuario = ? ";
}
function buildQuerySelectCategoriasPerfil()
{
    return "SELECT * FROM gc_usuario_categoria WHERE id_usuario = ?";
}

function buildQueryActualizarPerfil()
{
    return "UPDATE `gc_usuarios` SET `tx_correo`= ?,`tx_nombre`= ?,`tx_apellido`= ?,`tx_rut`= ?,`tx_descripcion`= ?,`tx_telefono`= ?,`tx_minsal`= ? WHERE id_usuario = ?";
}

function buildQueryActualizarPassword()
{
    return "UPDATE `gc_usuarios` SET `tx_password`= ? WHERE id_usuario = ?";
}

// Funciones de Configuración calendario.

function insertDiaCalendario(){
    return "INSERT INTO gc_calendar_conf_usuario (id_usuario, id_dia, hora_inicio, hora_fin) VALUES (?, ?, ?, ?)";
};

function deleteDiaCalendario(){
    return "DELETE FROM gc_calendar_conf_usuario WHERE id_usuario = ? AND id_dia = ?";
};

function selectDiaCalendarioUser(){
    return "SELECT * FROM gc_calendar_conf_usuario WHERE id_usuario = ? AND id_dia = ?";
}


// Front profesional info

function selectInfoProfesional(){
    return "SELECT id_usuario, tx_nombre, tx_apellido, tx_descripcion, id_rol, id_status, imagen_perfil FROM gc_usuarios WHERE id_rol = ? AND id_status = ?";
}

function selectCategoriaProfesional(){
    return "SELECT gc.*, guc.* FROM gc_usuario_categoria as guc, gc_categorias as gc WHERE gc.id_categoria = guc.id_categoria AND guc.id_usuario = ?";
}

function selectInfoProfesionalId(){
    return "SELECT id_usuario, tx_nombre, tx_apellido, tx_descripcion, id_rol, id_status, imagen_perfil FROM gc_usuarios WHERE id_rol = ? AND id_status = ? AND id_usuario = ?";
}

function selectDaysActiveProfesional(){
    return "SELECT DISTINCT id_dia FROM gc_calendar_conf_usuario WHERE id_usuario = ?";
}


// Front registro usuario
function insertCliente(){
    return "INSERT INTO gc_usuarios (tx_correo, tx_password, tx_nombre, tx_apellido, tx_rut, id_rol, id_status) VALUES (?, ? , ? ,?, ?, ?, ?)";
}

function selectCorreo(){
    return "SELECT tx_correo FROM gc_usuarios WHERE tx_correo = ? AND id_status != ?";
}

function savePrice(){
    return "UPDATE gc_usuario_categoria SET precio_servicio = ? WHERE id_usuario = ? AND id_categoria = ?";
}


// checkout Categoría

function getCategoriaPrecio(){
    return "SELECT gc.nb_categoria, gcu.precio_servicio FROM gc_categorias as gc, gc_usuario_categoria as gcu WHERE gc.id_categoria = gcu.id_categoria AND gcu.id_usuario = ? AND gcu.id_categoria = ?";
}

function saveReserva(){
    return "INSERT INTO gc_reserva (id_cliente, id_profesional, fecha_reserva, hora_inicio, hora_fin, id_especializacion, id_prevision, id_motivo, total_consulta, id_dia,id_status, cod_trans) VALUES (?, ?,?, ?, ?, ?,? ,?,?,?,?,?)";
}

function getTimesDateReserva(){
    return "SELECT * FROM gc_reserva WHERE fecha_reserva = ? AND id_profesional = ?";
}

function selectTimeFin(){
    return "SELECT ADDTIME(?, '00:30:00') as hora_fin";
}

function selectReservaCliente(){
    return "SELECT gr.*, ADDTIME(gr.hora_fin, '00:30:00') as hora_fin_visual, gu.tx_nombre, gu.tx_apellido,gc.nb_categoria  FROM gc_reserva as gr, gc_usuarios as gu, gc_categorias as gc WHERE id_cliente = ? AND fecha_reserva >= ? AND gu.id_usuario = gr.id_profesional AND gc.id_categoria = gr.id_prevision ORDER BY fecha_reserva, hora_inicio ASC";
}


function selectReservaProfesional(){
    return "SELECT gr.*, ADDTIME(gr.hora_fin, '00:30:00') as hora_fin_visual, gu.tx_nombre, gu.tx_apellido,gu.tx_rut,gc.nb_categoria  FROM gc_reserva as gr, gc_usuarios as gu, gc_categorias as gc WHERE id_profesional = ? AND fecha_reserva >= ? AND gu.id_usuario = gr.id_cliente AND gc.id_categoria = gr.id_prevision ORDER BY fecha_reserva, hora_inicio ASC";
}