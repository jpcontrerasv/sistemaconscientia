<?php 

include 'commons/funciones.php';
include 'commons/schema.php';

$db = connectToDB($dbData);

include 'header.php' ?>
<?php include 'topnav-front.php' 


?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">


    <!-- seccion modelos recientes -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-5 text-center">
                <h1 class="display-4">Centro de atención psicológica</h1>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="card shadow-none p-2">
                    <form>
                        <div class="form-row align-items-center justify-content-between">
                            <div class="col-6 form-inline">
                                <div class="input-group">
                                    <div class="input-group-prepend mb-0">
                                        <div class="input-group-text">🔍</div>
                                    </div>
                                    <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Buscar por nombre ⏎">
                                </div>


                            </div>

                            <div class="col-4 py-1">
                                <select id="choices-multiple-remove-button" placeholder="Seleccione las especialiades que desea filtrar" multiple>
                                    <option value="Psicólogo">Psicólogo</option>
                                    <option value="Psiquiatra">Psiquiatra</option>
                                    <option value="Coach">Coach</option>
                                </select>
                            </div>



                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">

            <?php include 'profile-items.php' ?>

        </div>
    </div>



    <!--sección -->
    <!--Fin contenido home-->

    <?php include 'footer-line.php' ?>

    <?php include 'footer.php' ?>