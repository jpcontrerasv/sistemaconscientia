<?php 
session_start(); 
 if(!isset($_SESSION["login"]) && $_SESSION["login"] != "ok"){  
        header("Location: login.php");
        die();
  }  


include 'header.php' ?>

<body class="nav-fixed">

    <?php include 'topnav.php' ?>

    <div id="layoutSidenav">

        <?php include 'sidebar.php' ?>

        <div id="layoutSidenav_content">
            <main>
                <header class="page-header">
                    <div class="container">
                        <div class="page-header-content pt-4">
                            <div class="row align-items-center justify-content-between">
                                <div class="col-auto mt-4">
                                    <h1 class="page-header-title">
                                        <div class="page-header-icon"><i data-feather="layout"></i></div>
                                        Administración
                                    </h1>
                                    <div class="page-header-subtitle">Elija un menú para comenzar.</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </header>
            </main>
            <?php include 'footer-line.php' ?>
        </div>
    </div>

    <?php include 'footer.php' ?>