<?php 
session_start();
include 'commons/funciones.php';
include 'commons/schema.php';
$db = connectToDB($dbData);

$id_cliente = $_GET["id_cliente"];
$id_profesional = $_GET["id_profesional"];
$fecha_reserva = $_GET["fecha_reserva"];
$hora_reserva = $_GET["hora_inicio"];

$sumar_minutos = $db->prepare(selectTimeFin());
$sumar_minutos->execute(array($hora_reserva));
$result_minutos = $sumar_minutos->fetch(PDO::FETCH_ASSOC);
$hora_fin = $result_minutos["hora_fin"]; // consultar en bd y añadirle solo 30 minutos

$dia_reserva = $_GET["id_dia"];
$categoria = $_GET["id_categoria"];
$prevision = $_GET["id_prevision"];
$motivo = $_GET["id_motivo"];
$precio = $_GET["total_consulta"];
$status = $_GET["id_status"];
$cod_transaccion = $_GET["cod_transaccion"];

$dia = explode("-", $fecha_reserva)[2];
$mes = explode("-", $fecha_reserva)[1];
$anio = explode("-", $fecha_reserva)[0];
//id_cliente, id_profesional, fecha_reserva, hora_inicio, hora_fin, id_especializacion, id_prevision, id_motivo, total_consulta, id_dia,id_status, cod_trans
$meses = [
    "01" => "enero",
    "02" => "febrero",
    "03" => "marzo",
    "04" => "abril",
    "05" => "mayo",
    "06" => "junio",
    "07" => "julio",
    "08" => "agosto",
    "09" => "septiembre",
    "10" => "octubre",
    "11" => "noviembre",
    "12" => "diciembre",
];



$stmta_prof = $db->prepare(selectInfoProfesionalId());
$stmta_prof->execute(array(2, 1, $id_profesional));
$result_profesional = $stmta_prof->fetch(PDO::FETCH_ASSOC);


$stmta_cat = $db->prepare(getCategoriaPrecio());
$stmta_cat->execute(array($id_profesional, $categoria));
$result_cat = $stmta_cat->fetch(PDO::FETCH_ASSOC);



$stmta_prev = $db->prepare(getCategoriaPrecio());
$stmta_prev->execute(array($id_profesional, $prevision));
$result_prev = $stmta_prev->fetch(PDO::FETCH_ASSOC);


$stmta = $db->prepare(saveReserva());
$stmta->execute(array($id_cliente,$id_profesional,$fecha_reserva,$hora_reserva,$hora_fin,$categoria,$prevision,$motivo,$precio,$dia_reserva,$status,$cod_transaccion));
//$result_profesional = $stmta->fetch(PDO::FETCH_ASSOC);





include 'header.php' ?>
<?php include 'topnav-front.php' ?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-5 text-center">
                <i class="fas fa-check fa-3x color-bermuda mb-4"></i>
                <h1 class="display-4">Tu hora está reservada</h1>
<br>
                
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card p-4 mb-5 shadow-none">

                    <div class="card-body">

                        <!--tabla checkout-->
                        <div class="datatable mb-4">
                            <table class="table table-bordered table-hover rounded" id="tableCarrito" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Cuándo</th>
                                        <th>Profesional</th>
                                        <th>Servicio</th>
                                        <th>Previsión</th>

                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody class="bg-white">
                                    <tr>
                                        <td><?php echo $dia . " de " . $meses[$mes] . " de " . $anio . " - " . $hora_reserva;  ?></td>
                                        
                                        <td><?php echo utf8_encode($result_profesional["tx_nombre"] . " " . $result_profesional["tx_apellido"]); ?></td>
                                        <td><?php echo ($result_cat["nb_categoria"]); ?></td>
                                        <td><?php echo ($result_prev["nb_categoria"]); ?></td>
                                        <td><span class="font-bold">$<?php echo $precio; ?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!--link perfil-->

                    </div>

                    <div class="card-footer text-center">
                        <p>Puedes administrar tus horas en tu perfil</p>
                        <a href="login.php" class="btn btn-lg btn-primary btn-register">Ir a mi perfil</a>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--Fin contenido home-->
    <?php include 'footer-line.php' ?>
    <?php include 'footer.php' ?>