<?php include 'header.php' ?>
<?php include 'topnav-front.php' ?>

<!-- Inicio contenido home -->

<body id="home" class="nav-fixed">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-5 text-left">
                <h1 class="rock-font display-4 color-granate">Política de privacidad</h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card p-4 mb-5">
                    <div class="card-header mb-3 d-flex justify-content-end p-2">
                        

                        <p class="text-muted m-0"><small>Última actualización <?php echo date("d M Y"); ?> <?php echo "" . date("H:i"); ?></small>
                    </div>

                    <div class="card-body">

                        <p>
                            En Grupo Conscientia sabemos que te preocupa cómo se utiliza tu información personal y como es compartida, es por esto que nos tomamos en serio tu privacidad. Por favor lee lo siguiente para saber más acerca de nuestra política de privacidad. Al visitar la página web http://grupoconscientia.cl y el dominio (colectivamente, el "Sitio Web"), y/o el uso de otras páginas vinculadas, características especiales, contenido o servicios de aplicaciones que ofrece de vez en cuando Grupo Conscientia (colectivamente el "Servicio"), aceptas las prácticas y las políticas descritas en esta Política de Privacidad.
                        </p>
                        <ol>
                            <li>
                                <h4>¿Qué cubre esta Política de Privacidad?</h4>
                                <p>
                                    Esta Política de Privacidad cubre el trato que Grupo Conscientia da a la información de identificación personal del usuario que reúne cuando este tiene acceso al Sitio Web y/o utiliza algunas otras características del Servicio. Esta política no se aplica a las prácticas de las empresas que Grupo Conscientia no posee o controla, o a las personas que Grupo Conscientia no emplea ni administra.
                                </p>
                            </li>
                            <li>
                                <h4>¿Qué información personal recopila Grupo Conscientia?</h4>
                                <p>
                                    La información que recopilamos nos permite personalizar y mejorar nuestros servicios. Recopilamos los siguientes tipos de información de nuestros usuarios:
                                <ol>
                                    <li>
                                        <h5>Información personal que el usuario proporciona</h5>
                                        <p>
                                            Recibimos y almacenamos cualquier información que el usuario introduce en el Sitio Web o nos proporciona de cualquier otra manera. Los tipos de información personal recopilada incluye su nombre completo, dirección de correo electrónico, dirección IP, información del navegador, nombre de usuario, contraseña y cualquier otra información necesaria para que podamos proporcionar el Servicio, incluyendo los nombres de usuario y contraseñas para sus cuentas y perfiles en sitios de terceros con la que trabaja el Servicio. El usuario puede optar por no proporcionarnos cierta información, pero entonces no podrá usar muchas de nuestras características especiales.
                                        </p>
                                    </li>
                                    <li>
                                        <h5>Información personal recopilada automáticamente</h5>
                                        <p>
                                            a) Recibimos y almacenamos ciertos tipos de información cuando el usuario interactúa con nuestro Sitio Web o utiliza otra característica de nuestro Servicio. Grupo Conscientia recibe y registra automáticamente información en nuestros registros del servidor desde su navegador, incluyendo su dirección IP, información de cookies y la página que el usuario solicitó.
                                        </p>
                                        <p>
                                            b) De manera más general, nuestro servicio recopila automáticamente información de uso, tales como el número y la frecuencia de visitantes a nuestro sitio web y de sus componentes, o los datos de preferencia del usuario. Grupo Conscientia sólo utiliza estos datos en forma agregada, es decir, como una medida estadística, y no en una forma que pueda identificar al usuario personalmente. Este tipo de datos agregados nos permite averiguar con qué frecuencia los clientes utilizan partes del Sitio Web o de otra de las características del servicio, para que podamos hacer el sitio web atractivo a tantos clientes como sea posible, y mejorar el Servicio. Como parte de este uso de la información, es posible proporcionar información agregada a nuestros socios sobre cómo nuestros clientes, colectivamente, utilizan nuestro servicio. Compartimos este tipo de datos estadísticos para que nuestros socios también entiendan con qué frecuencia las personas usan nuestro servicio, para que ellos, también, pueden proporcionarle una experiencia online óptima. Grupo Conscientia no revela información agregada a socios de manera que pudiera identificar a un usuario personalmente.
                                        </p>
                                    </li>
                                    
                                </ol>
                                </p>
                            </li>
                            <li>
                                <h4>¿Qué sucede con la Cookies?</h4>
                                <p>
                                    Las cookies son identificadores alfanuméricos que transferimos al disco duro del usuario a través de su navegador, para permitir a nuestros sistemas reconocer su navegador y decirnos cómo y cuándo las páginas de nuestro sitio son visitadas y por cuántas personas. Utilizamos cookies para mejorar la experiencia, para aprender más acerca de su uso del Sitio Web y para mejorar la calidad.
                                </p>
                                <p>
                                    La mayoría de los navegadores tienen una opción para desactivar la función de cookies, lo que evitará que su navegador acepte cookies nuevas, así como (dependiendo de la sofisticación de su navegador) le permite decidir sobre la aceptación de cada nueva cookie en una variedad de formas. Le recomendamos que deje las cookies activadas, ya que las cookies permiten que el usuario se beneficie de algunas de las características más atractivas de la Web.
                                </p>
                                <p>
                                    Los anuncios que aparecen en el Sitio Web o en otras características del Servicio pueden ser entregados a los usuarios por nuestros socios de publicidad, que pueden establecer cookies. Estas cookies permiten al servidor de anuncios reconocer su equipo cada vez que le envían una publicidad en línea para recopilar información acerca del usuario o de otras personas que utilizan el ordenador. Esta información permite a los anunciantes, entre otras cosas, realizar determinados anuncios que ellos creen que serán de mayor interés para el usuario. Esta política de privacidad cubre el uso de cookies, mediante Grupo Conscientia y no cubre el uso de cookies por parte de anunciantes.
                                </p>
                            </li>
                            <li>
                                <h4>¿Compartiremos tu información?</h4>
                                <p>
                                    La información personal de nuestros clientes y usuarios es una parte integral de nuestro negocio. Nosotros no arrendamos ni vendemos la información personal a nadie. Compartimos su información personal solo como se describe a continuación.

                                <ol>
                                    <li>
                                        <h5>Agentes</h5>
                                        <p>
                                            Empleamos a otras compañías y personas para realizar tareas en nuestro nombre y necesitamos compartir su información con ellos para proporcionar productos o servicios a los usuarios. <strong class="text-danger">Las conversaciones personales del usuario jamás serán compartidas</strong>. A menos que le indiquemos lo contrario, los agentes de Grupo Conscientia no tienen ningún derecho a utilizar la información personal que compartimos con ellos más allá de lo necesario para que nos ayuden. El usuario acepta compartir la información personal para los fines antes mencionados.
                                        </p>
                                    </li>
                                    <li>
                                        <h5>Comunicación en respuesta la información cargada por el usuario</h5>
                                        <p>
                                            Como parte del Servicio, el usuario recibirá de email Grupo Conscientia. El usuario reconoce y acepta que mediante la publicación de dichos Archivos de Usuario, Grupo Conscientia puede enviarle por email y otro tipo de comunicación que determine a su sola discreción, emails relacionados a dicho contenido subido por el usuario.
                                        </p>
                                    </li>
                                    <li>
                                        <h5>Protección de Grupo Conscientia y Otros</h5>
                                        <p>
                                            Podemos divulgar Información Personal cuando creamos de buena fe que es necesario para cumplir con la ley; hacer cumplir o aplicar nuestras condiciones de uso y otros acuerdos; o proteger los derechos, propiedad o seguridad de Grupo Conscientia, nuestros empleados, nuestros usuarios, u otros. Esto incluye el intercambio de información con otras compañías y organizaciones para protección la del fraude y reducción de riesgo de crédito.
                                        </p>
                                    </li>
                                    <li>
                                        <h5>Con su consentimiento</h5>
                                        <p>
                                            A excepción de lo establecido anteriormente, se le notificará cuando la información personal puede ser compartida con terceros, y será capaz de evitar el intercambio de esta información.
                                        </p>
                                    </li>
                                </ol>
                                </p>
                            </li>
                            <li>
                                <h4>¿Está mi información personal segura?</h4>
                                <p>
                                    La información personal de una Cuenta Grupo Conscientia está protegida por una contraseña para su privacidad y seguridad. El usuario necesita asegurarse de que no hay un acceso no autorizado a su cuenta e información personal mediante la selección y la adecuada protección de la contraseña y limitar el acceso a su ordenador y el navegador mediante la cerrar la sesión después de que haya terminado de acceder a su cuenta.
                                </p>
                                <p>
                                    Los esfuerzos de Grupo Conscientia están para proteger la información del usuario y garantizar que la información de cuenta de usuario se mantiene privada. Sin embargo, como en todo servicio basado en internet, Grupo Conscientia no puede garantizar totalmente la seguridad de la información de la cuenta de usuario. La entrada no autorizada o el uso, fallos de hardware o software, y otros factores, pueden comprometer la seguridad de la información.
                                </p>
                                <p>
                                    El sitio web contiene enlaces a otros sitios. Grupo Conscientia no es responsable de las políticas y/o prácticas de privacidad de otros sitios. Cuando se enlaza a otro sitio, el usuario debe leer la política de privacidad que aparece en dicho sitio. Esta política de privacidad sólo cubre la información recopilada en las plataformas pertenecientes a Grupo Conscientia.
                                </p>
                                <p>
                                    El usuario debe tener en cuenta que cuando publique voluntariamente información en las áreas públicas del sitio web, esta información podrá ser accedida por el público.
                                </p>
                            </li>
                            <li>
                                <h4>¿A qué información personal puedo acceder?</h4>
                                <p>
                                    Grupo Conscientia le permite acceder al usuario a la siguiente información con el propósito de ver, y en ciertas situaciones, actualizar dicha información. Esta lista cambiará a medida de que la plataforma cambie.
                                <ul>
                                    <li>
                                        Nombre Real
                                    </li>
                                    <li>
                                        Cuenta y el perfil de usuario de la información
                                    </li>
                                    <li>
                                        E-mail del usuario
                                    </li>
                                    <li>
                                        Nombre de usuario y la contraseña
                                    </li>
                                    <li>
                                        Teléfono del usuario
                                    </li>
                                </ul>
                                Grupo Conscientia nunca permitirá que esta información se difunda de manera pública. Podemos comenzar a exigir a los nuevos inscritos proporcionar alguna o toda la información anterior.
                                </p>
                            </li>
                            <li>
                                <h4>¿Qué opciones tengo?</h4>
                                <p>
                                    Como se dijo anteriormente, siempre se puede optar por no revelar información, a pesar de que puede ser necesaria para aprovechar características de la Web o del Servicio. El usuario es capaz de añadir o actualizar información de manera segura en páginas, como la que se enumera en la sección anterior.
                                </p>
                                <p>
                                    El usuario puede solicitar que se elimine su cuenta Grupo Conscientia siguiendo las instrucciones en la sección de ajustes de la cuenta de la página web. El usuario debe tener en cuenta que alguna información puede permanecer en nuestros archivos después de la eliminación de su cuenta.
                                </p>
                                <p>
                                    Cuando el usuario se registra en una cuenta en nuestro sitio Web, el usuario acepta recibir correo electrónico u otras comunicaciones de nosotros; si el usuario no desea recibir este tipo de comunicaciones, no debe crear una cuenta en nuestro sitio web. El usuario debe considerar que si elige no recibir avisos legales de nosotros, como la presente Política de Privacidad, los avisos legales seguirán rigiendo el uso del Sitio Web, y el usuario es responsable de revisar estos avisos legales para enterarse de los cambios.
                                </p>
                            </li>
                            <li>
                                <h4>Cambios en esta Política de Privacidad</h4>
                                <p>
                                    Grupo Conscientia podrá modificar esta Política de Privacidad de vez en cuando. El uso de la información que recopilamos ahora está sujeto a la política de privacidad vigente en el momento en que la información se utiliza. Si hacemos cambios en la forma en que usamos la información personal, le notificaremos publicando un anuncio en nuestra página web o mediante el envío de un correo electrónico. Los usuarios están regidos por cualquier cambio en la política de privacidad cuando él o ella utilizan el Sitio Web después que tales cambios se han publicado primero.
                                </p>
                            </li>
                            <li>
                                <h4>Dudas o sugerencias</h4>
                                <p>
                                    Si existe cualquier consulta o sugerencia respecto a la privacidad en Grupo Conscientia, por favor envíanos un correo a contacto@Grupo Conscientia-app.com. Haremos todos los esfuerzos para solucionar tus inquietudes.
                                </p>
                            </li>
                        </ol>
                        <p class='text-right'>
                            Última modificación: 9 de Noviembre de 2019
                        </p>


                    </div>
                </div>


            </div>
        </div>
    </div>

    <!--Fin contenido home-->
    <?php include 'footer-line.php' ?>
    <?php include 'footer.php' ?>